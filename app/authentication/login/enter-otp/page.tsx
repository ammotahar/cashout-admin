"use client";

import { useState, useEffect, useCallback } from "react";
import { useRouter } from "next/navigation";
import { toast } from "react-toastify";
import Button from "@/components/button";
import Otp from "@/components/otp";
import { verifyOtpApi, sendOtpLoginApi } from "@/services/authorization";

export default function EnterOtpPage() {
  const [showTimer, setShowTimer] = useState(true);
  const [otpCode, setOtpCode] = useState("");
  const [loading, setLoading] = useState(false);
  const [resendLoading, setResendLoading] = useState(false);

  const router = useRouter();

  async function resendOtp() {
    const username = localStorage.getItem("username");
    const password = localStorage.getItem("password");

    if (username && password) {
      setResendLoading(true);

      const res = await sendOtpLoginApi({
        username,
        password,
        is_admin: true,
      });

      setResendLoading(false);
      setShowTimer(true);
    }
  }

  const verifyOtp = useCallback(async () => {
    const username = localStorage.getItem("username");

    if (otpCode.length === 6) {
      setLoading(true);

      const res = await verifyOtpApi({
        username: username,
        otp: otpCode,
        is_admin: true,
      });

      if (res) {
        localStorage.setItem("access_token", res?.data?.access_token);
        localStorage.setItem("refresh_token", res?.data?.refresh_token);
        localStorage.removeItem("username");
        localStorage.removeItem("password");
        router.push("/panel");
      }

      setLoading(false);
    } else {
      toast.error("لطفا کد ارسال شده را وارد کنید.");
    }
  }, [otpCode, router]);

  useEffect(() => {
    if (otpCode.length === 6) {
      verifyOtp();
    }
  }, [otpCode, verifyOtp]);

  return (
    <div className="flex flex-col">
      <div className="mt-4">
        <div>
          <span className="text-subtitle text-xs">
            کد یکبار مصرف به شماره همراه شما ارسال شده است.
          </span>
        </div>

        <Otp
          onResendOtp={resendOtp}
          showTimer={showTimer}
          setShowTimer={setShowTimer}
          otpCode={otpCode}
          setOtpCode={setOtpCode}
          resendLoading={resendLoading}
        />
      </div>

      <Button
        className="!self-end !mt-10 !mb-4"
        onClick={verifyOtp}
        loading={loading}
      >
        ورود
      </Button>
    </div>
  );
}
