"use client";

import { useState } from "react";
import { useRouter } from "next/navigation";
import { useForm, Controller, SubmitHandler } from "react-hook-form";
import TextField from "@/components/textField";
import Checkbox from "@/components/checkbox";
import Button from "@/components/button";
import { PersonIcon } from "@/assets/icons/black/person";
import { PasswordIcon } from "@/assets/icons/black/password";
import { sendOtpLoginApi } from "@/services/authorization";

interface IFormInput {
  username: string;
  password: string;
}

export default function LoginPage() {
  const [loading, setLoading] = useState(false);

  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm({
    defaultValues: {
      username: "",
      password: "",
    },
  });

  const router = useRouter();

  const onSubmit: SubmitHandler<IFormInput> = async (data) => {
    setLoading(true);

    const res = await sendOtpLoginApi({ ...data, is_admin: true });

    setLoading(false);

    if (res) {
      localStorage.setItem("username", data.username);
      localStorage.setItem("password", data.password);
      router.push("/authentication/login/enter-otp");
    }
  };

  return (
    <form className="flex flex-col" onSubmit={handleSubmit(onSubmit)}>
      <div className="mt-4">
        <div>
          <span className="text-subtitle text-xs">
            نام کاربری و کلمه عبور به شماره همراه شما ارسال شده است.
          </span>
        </div>

        <Controller
          name="username"
          control={control}
          rules={{
            required: "نام کاربری را وارد نمایید.",
          }}
          render={({ field: { onChange, value } }) => (
            <TextField
              title="نام کاربری"
              value={value}
              onChange={onChange}
              icon={<PersonIcon />}
              className="mb-4 mt-8"
              error={!!errors?.username}
              helperText={errors?.username?.message}
              maxLength={50}
            />
          )}
        />

        <Controller
          name="password"
          control={control}
          rules={{
            required: "رمز عبور را وارد نمایید.",
          }}
          render={({ field: { onChange, value } }) => (
            <TextField
              title="کلمه عبور"
              value={value}
              onChange={onChange}
              icon={<PasswordIcon />}
              className="mb-4 mt-8"
              type="password"
              error={!!errors?.password}
              helperText={errors?.password?.message}
              maxLength={50}
            />
          )}
        />

        <div className="flex items-center justify-between mt-2">
          <Checkbox
            title="مرا به خاطر بسپار"
            titleClassName="text-xs text-subtitle"
          />

          {/* <Button variant="text">
            <Link href="/authentication/forgot-password">
              <span className="text-xs text-primary">فراموشی رمز عبور</span>
            </Link>
          </Button> */}
        </div>
      </div>

      <Button
        className="!self-end !mt-10 !mb-4"
        type="submit"
        loading={loading}
      >
        مرحله بعد
      </Button>
    </form>
  );
}
