import type { Metadata } from "next";
import { ToastContainer } from "react-toastify";
import CThemeProvider from "theme/theme-provider";
import ContextProvider from "@/context/provider";
import "./globals.css";
import "react-toastify/dist/ReactToastify.css";

export const metadata: Metadata = {
  title: "Admin CashoutV2",
  description: "Admin CashoutV2",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="fa">
      <body>
        <ContextProvider>
          <CThemeProvider>{children}</CThemeProvider>
          <ToastContainer rtl />
        </ContextProvider>
      </body>
    </html>
  );
}
