"use client";

import { useState, useEffect, useCallback } from "react";
import { useSearchParams, useRouter } from "next/navigation";
import { useForm, SubmitHandler, Controller } from "react-hook-form";
import { toast } from "react-toastify";
import PanelBox from "@/components/panelBox";
import MaterialTextField from "@/components/materialTextField";
import Select from "@/components/select";
import Radio from "@/components/radio";
import Checkbox from "@/components/checkbox";
import Button from "@/components/button";
import IbanInfoModal from "@/components/ibanInfoModal";
import {
  createAccountApi,
  getAccountDetailsApi,
  editAccountApi,
} from "@/services/account";
import { AccountDetailsType } from "@/services/account/types";
import { getBankListApi } from "@/services/bank";
import { BankListType } from "@/services/bank/types";

interface IFormInput {
  description?: string;
  user_name: string;
  password: string;
  account_name?: string;
  full_name: string;
  is_active?: boolean;
  account_number: string;
  iban: string;
  customer_code?: string;
  view_dashboard?: boolean;
  account_number_inq?: string;
  first_name_inq?: string;
  account_status?: string;
  bank_inq?: string;
  channel_name: string;
  secret_key: string;
  bank: number | string;
}

export default function CreateAccountPage() {
  const [loading, setLoading] = useState(false);
  const [bankList, setBankList] = useState<BankListType[]>([]);
  const [ibanInfoOpen, setIbanInfoOpen] = useState(false);

  const router = useRouter();

  const searchParams = useSearchParams();

  const accountId = searchParams.get("id");

  const {
    handleSubmit,
    control,
    setValue,
    getValues,
    trigger,
    getFieldState,
    reset,
    formState: { errors },
  } = useForm<IFormInput>({
    defaultValues: {
      description: "",
      user_name: "",
      password: "",
      account_name: "",
      full_name: "",
      is_active: true,
      account_number: "",
      iban: "",
      customer_code: "",
      view_dashboard: true,
      account_number_inq: "",
      first_name_inq: "",
      account_status: "",
      bank_inq: "",
      channel_name: "",
      secret_key: "",
      bank: "",
    },
  });

  async function getBankList() {
    const res = await getBankListApi();
    if (res) {
      setBankList(res?.data);
    }
  }

  useEffect(() => {
    getBankList();
  }, []);

  const getAccountDetails = useCallback(
    async (id: number) => {
      setLoading(true);

      const res = await getAccountDetailsApi(id);

      if (res) {
        const data: AccountDetailsType = res?.data;

        setValue("description", data.description);
        setValue("user_name", data.user_name);
        setValue("password", data.password);
        setValue("account_name", data.account_name);
        setValue("full_name", data.full_name);
        setValue("is_active", data.is_active);
        setValue("account_number", data.account_number);
        setValue("iban", data.iban.split("IR")[1]);
        setValue("customer_code", data.customer_code);
        setValue("view_dashboard", data.view_dashboard);
        setValue("account_number_inq", data.account_number_inq);
        setValue("first_name_inq", data.first_name_inq);
        setValue("account_status", data.account_status);
        setValue("bank_inq", data.bank_inq);
        setValue("channel_name", data.channel_name);
        setValue("secret_key", data.secret_key);
        setValue("bank", data.bank_id);
      }

      setLoading(false);
    },
    [setValue]
  );

  useEffect(() => {
    if (accountId) {
      getAccountDetails(parseInt(accountId));
    }
  }, [getAccountDetails, accountId]);

  const onSubmit: SubmitHandler<IFormInput> = async (data) => {
    setLoading(true);

    if (accountId) {
      const res = await editAccountApi(
        {
          ...(data.description && { description: data.description }),
          user_name: data.user_name,
          password: data.password,
          ...(data.account_name && { account_name: data.account_name }),
          full_name: data.full_name,
          is_active: data.is_active,
          account_number: data.account_number,
          iban: `IR${data?.iban}`,
          ...(data.customer_code && { customer_code: data.customer_code }),
          view_dashboard: data.view_dashboard,
          ...(data.account_number_inq && {
            account_number_inq: data.account_number_inq,
          }),
          ...(data.first_name_inq && { first_name_inq: data.first_name_inq }),
          ...(data.account_status && { account_status: data.account_status }),
          ...(data.bank_inq && { bank_inq: data.bank_inq }),
          channel_name: data.channel_name,
          secret_key: data.secret_key,
          bank: typeof data.bank !== "string" ? data.bank : 0,
        },
        parseInt(accountId)
      );
      if (res) {
        router.back();
        toast.success("حساب با موفقیت ویرایش شد");
      }
    } else {
      const res = await createAccountApi({
        ...(data.description && { description: data.description }),
        user_name: data.user_name,
        password: data.password,
        ...(data.account_name && { account_name: data.account_name }),
        full_name: data.full_name,
        is_active: data.is_active,
        account_number: data.account_number,
        iban: `IR${data?.iban}`,
        ...(data.customer_code && { customer_code: data.customer_code }),
        view_dashboard: data.view_dashboard,
        ...(data.account_number_inq && {
          account_number_inq: data.account_number_inq,
        }),
        ...(data.first_name_inq && { first_name_inq: data.first_name_inq }),
        ...(data.account_status && { account_status: data.account_status }),
        ...(data.bank_inq && { bank_inq: data.bank_inq }),
        channel_name: data.channel_name,
        secret_key: data.secret_key,
        bank: typeof data.bank !== "string" ? data.bank : 0,
      });

      if (res) {
        reset();
        toast.success("حساب با موفقیت ثبت شد");
      }
    }

    setLoading(false);
  };

  return (
    <PanelBox>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="grid grid-cols-1 gap-4 xl:grid-cols-3">
          <Controller
            name="account_name"
            control={control}
            rules={{
              required: "عنوان حساب را وارد نمایید.",
            }}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="عنوان حساب *"
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1"
                error={!!errors?.account_name}
                helperText={errors?.account_name?.message}
              />
            )}
          />

          <Controller
            name="full_name"
            control={control}
            rules={{
              required: "نام صاحب حساب را وارد نمایید.",
            }}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="نام صاحب حساب *"
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1"
                error={!!errors?.full_name}
                helperText={errors?.full_name?.message}
              />
            )}
          />

          <Controller
            name="account_number"
            control={control}
            rules={{
              required: "شماره حساب را وارد نمایید.",
            }}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="شماره حساب *"
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1"
                error={!!errors?.account_number}
                helperText={errors?.account_number?.message}
              />
            )}
          />

          <Controller
            name="iban"
            control={control}
            rules={{
              required: "شماره شبا را وارد نمایید.",
              pattern: {
                value: /^(?=.{24}$)[0-9]*$/gi,
                message: "شماره شبا وارد شده صحیح نیست",
              },
            }}
            render={({ field: { onChange, value } }) => (
              <div className="col-span-1 flex items-center gap-2">
                <MaterialTextField
                  label="شماره شبا *"
                  value={value}
                  onChange={onChange}
                  startAdornments="IR"
                  className="!bg-white !w-2/3"
                  error={!!errors?.iban}
                  helperText={errors?.iban?.message}
                />

                <Button
                  padding="!px-0"
                  fullRounded={false}
                  className="!w-1/3 !text-xs !h-full"
                  onClick={async () => {
                    await trigger("iban");
                    if (!getFieldState("iban").invalid) {
                      setIbanInfoOpen(true);
                    } else {
                      toast.error("ابتدا شماره شبا را به درستی وارد نمایید");
                    }
                  }}
                >
                  استعلام شماره شبا
                </Button>
              </div>
            )}
          />

          <Controller
            name="customer_code"
            control={control}
            rules={{
              required: "شماره مشتری را وارد نمایید.",
            }}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="شماره مشتری *"
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1"
                error={!!errors?.customer_code}
                helperText={errors?.customer_code?.message}
              />
            )}
          />

          <Controller
            name="bank"
            control={control}
            rules={{
              required: "نام بانک را انتخاب نمایید.",
            }}
            render={({ field: { onChange, value } }) => (
              <Select
                label="نام بانک *"
                className="!col-span-1"
                onChange={onChange}
                value={value}
                items={bankList}
                error={!!errors?.bank}
                helperText={errors?.bank?.message}
              />
            )}
          />

          <Controller
            name="user_name"
            control={control}
            rules={{
              required: "شناسه کاربری را وارد نمایید.",
            }}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="شناسه کاربری *"
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1"
                error={!!errors?.user_name}
                helperText={errors?.user_name?.message}
              />
            )}
          />

          <Controller
            name="password"
            control={control}
            rules={{
              required: "کلمه عبور را وارد نمایید.",
            }}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="کلمه عبور *"
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1"
                error={!!errors?.password}
                helperText={errors?.password?.message}
              />
            )}
          />

          <Controller
            name="secret_key"
            control={control}
            rules={{
              required: "کلید رمز را وارد نمایید.",
            }}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="کلید رمز (Secret Key) *"
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1"
                error={!!errors?.secret_key}
                helperText={errors?.secret_key?.message}
              />
            )}
          />

          <Controller
            name="channel_name"
            control={control}
            rules={{
              required: "نام کانال را وارد نمایید.",
            }}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="نام کانال (Channel Name) *"
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1"
                error={!!errors?.channel_name}
                helperText={errors?.channel_name?.message}
              />
            )}
          />

          <Controller
            name="view_dashboard"
            control={control}
            render={({ field: { onChange, value } }) => (
              <Checkbox
                title="امکان مشاهده در داشبورد"
                value={value}
                onChange={onChange}
              />
            )}
          />

          <Controller
            name="is_active"
            control={control}
            render={({ field: { onChange, value } }) => (
              <div className="xl:flex xl:items-center">
                <div>
                  <span>وضعیت حساب :</span>
                </div>

                <div className="flex items-center justify-center gap-8 bg-white rounded mt-2 xl:bg-transparent xl:mt-0 xl:mr-6">
                  <Radio
                    label="فعال"
                    checked={value === true}
                    handleChange={() => onChange(true)}
                  />
                  <Radio
                    label="غیر فعال"
                    checked={value === false}
                    handleChange={() => onChange(false)}
                  />
                </div>
              </div>
            )}
          />

          <Controller
            name="description"
            control={control}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="توضیحات"
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1 xl:!col-span-3"
                multiline
              />
            )}
          />
        </div>

        <div className="grid grid-cols-2 lg:grid-cols-8 gap-4 mt-8 w-full">
          <Button
            fullRounded={false}
            className="!col-span-1 lg:!col-start-7"
            type="submit"
            loading={loading}
          >
            {accountId ? "ویرایش" : "ثبت"}
          </Button>
          <Button
            variant="cancel"
            className="!col-span-1 lg:!col-start-8"
            loading={loading}
            onClick={() => (accountId ? router.back() : router.push("/panel"))}
          >
            انصراف
          </Button>
        </div>
      </form>

      <IbanInfoModal
        open={ibanInfoOpen}
        onClose={() => setIbanInfoOpen(false)}
        iban={`IR${getValues("iban")}`}
        onCompleteInquiry={(data) => {
          setValue("account_number_inq", data.account_number);
          setValue("first_name_inq", `${data.first_name} ${data.last_name}`);
        }}
        onConfirmData={(data) => {
          setValue("account_number", data.account_number);
          setValue("full_name", `${data.first_name} ${data.last_name}`);
        }}
      />
    </PanelBox>
  );
}
