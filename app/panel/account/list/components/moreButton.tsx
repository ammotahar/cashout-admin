"use client";

import { useState } from "react";
import { useRouter } from "next/navigation";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Button from "@/components/button";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";

export default function MoreButton({
  accountId,
  onViewDetailsClick,
  onDeleteClick,
  onDeactiveClick,
}: {
  accountId: number;
  onViewDetailsClick?: () => void;
  onDeleteClick?: () => void;
  onDeactiveClick?: () => void;
}) {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const open = Boolean(anchorEl);

  const router = useRouter();

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <Button variant="icon" onClick={handleClick}>
        <MoreHorizIcon />
      </Button>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          "aria-labelledby": "basic-button",
        }}
      >
        <MenuItem
          onClick={() => {
            onViewDetailsClick && onViewDetailsClick();
            handleClose();
          }}
          className="!text-primary"
        >
          مشاهده جزئیات
        </MenuItem>

        <MenuItem
          onClick={() => {
            router.push(`/panel/account/create?id=${accountId}`);
            handleClose();
          }}
          className="!text-success"
        >
          ویرایش
        </MenuItem>

        <MenuItem
          onClick={() => {
            onDeleteClick && onDeleteClick();
            handleClose();
          }}
          className="!text-error"
        >
          حذف
        </MenuItem>

        <MenuItem
          onClick={() => {
            onDeactiveClick && onDeactiveClick();
            handleClose();
          }}
          className="!text-yellow"
        >
          غیر فعال کردن
        </MenuItem>
      </Menu>
    </>
  );
}
