"use client";

import { useState, useEffect } from "react";
import Image from "next/image";
import { ExpandLess, ExpandMore } from "@mui/icons-material";
import PanelBox from "@/components/panelBox";
import SearchField from "@/components/searchField";
import Button from "@/components/button";
import Table from "@/components/table";
import Pagination from "@/components/pagination";
import AccountDetailsModal from "@/components/accountDetailsModal";
import DeleteOrDeactiveAccountModal from "@/components/deleteAccountModal";
import Filter from "../components/filter";
import MoreButton from "./components/moreButton";
import { getAccountListApi, getAccountListExcelApi } from "@/services/account";
import { AccountListType } from "@/services/account/types";
import FilterIcon from "assets/icons/filter.svg";
import ExcelIcon from "assets/icons/excel.svg";

export default function AccountsListPage() {
  const [search, setSearch] = useState("");
  const [filterOpen, setFilterOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<AccountListType[]>([]);
  const [count, setCount] = useState(0);
  const [page, setPage] = useState(1);
  const [excelDownloadLoading, setExcelDownloadLoading] = useState(false);
  const [accountName, setAccountName] = useState("");
  const [accountNumber, setAccountNumber] = useState("");
  const [bankName, setBankName] = useState("");
  const [iban, setIban] = useState("");
  const [accountDetailsModalOpen, setAccountDetailsModalOpen] = useState(false);
  const [accountId, setAccountId] = useState(0);
  const [deleteAccountModalOpen, setDeleteAccountModalOpen] = useState(false);
  const [deleteAccountModalType, setDeleteAccountModalType] = useState<
    "delete" | "deactive"
  >("delete");

  async function getAccountList(
    page?: number,
    search?: string,
    account_name?: string,
    account_number?: string,
    bank_name?: string,
    iban?: string
  ) {
    setLoading(true);

    const res = await getAccountListApi({
      per_page: 10,
      page_number: page,
      search,
      account_name,
      account_number,
      bank_name,
      ...(iban && { iban: `IR${iban}` }),
    });

    if (res) {
      setData(res?.data?.data);
      setCount(res?.data?.count);
    }

    setLoading(false);
  }

  useEffect(() => {
    getAccountList(1);
  }, []);

  // useEffect(() => {
  //   setPage(1);
  //   getAccountList(1, search, accountName, accountNumber, bankName, iban);
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [search]);

  function pageHandler(event: any, page: number) {
    setPage(page);
    getAccountList(page, search, accountName, accountNumber, bankName, iban);
  }

  async function getAccountListExcel(
    search?: string,
    account_name?: string,
    account_number?: string,
    bank_name?: string,
    iban?: string
  ) {
    setExcelDownloadLoading(true);

    const res = await getAccountListExcelApi({
      search,
      account_name,
      account_number,
      bank_name,
      ...(iban && { iban: `IR${iban}` }),
    });
    const blob = new Blob([res], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    });
    let url = window.URL.createObjectURL(blob);
    let a = document.createElement("a");
    a.href = url;
    a.download = "accounts_list.xlsx";
    a.click();
    setExcelDownloadLoading(false);
  }

  function deleteFilters() {
    setPage(1);
    setAccountName("");
    setAccountNumber("");
    setBankName("");
    setIban("");
    getAccountList(page, search);
  }

  return (
    <PanelBox>
      <div className="w-full flex flex-col items-end gap-4 lg:flex-row lg:justify-between lg:items-center mb-4">
        <div className="flex items-center gap-4 w-full lg:w-1/2">
          {/* <SearchField
            placeholder="نام شرکت"
            onChange={(value) => setSearch(value)}
          /> */}

          <Button
            className="!flex !items-center !justify-center lg:!justify-between lg:!px-2 !bg-orange !rounded-md !min-w-10 !min-h-10 lg:!w-32"
            variant="text"
            onClick={() => setFilterOpen(!filterOpen)}
          >
            <div className="flex items-center gap-1">
              <Image src={FilterIcon} alt="filter" />
              <span className="hidden lg:block text-xs text-white">فیلتر</span>
            </div>

            <div className="hidden lg:block">
              {filterOpen ? (
                <ExpandLess className="!text-xl !text-white" />
              ) : (
                <ExpandMore className="!text-xl !text-white" />
              )}
            </div>
          </Button>
        </div>

        <Button
          fullRounded={false}
          className="!col-span-1 !flex !items-center !justify-center !gap-2 !min-h-10 !bg-triatary !border !border-green !border-solid"
          padding="!px-2"
          onClick={() =>
            getAccountListExcel(
              search,
              accountName,
              accountNumber,
              bankName,
              iban
            )
          }
          loading={excelDownloadLoading}
        >
          <span className="text-xs text-black">خروجی اکسل</span>
          <Image src={ExcelIcon} alt="export" />
        </Button>
      </div>

      <Filter
        open={filterOpen}
        onClose={() => setFilterOpen(false)}
        onAccountNameChange={(event) => setAccountName(event.target.value)}
        accountName={accountName}
        onAccountNumberChange={(event) => setAccountNumber(event.target.value)}
        accountNumber={accountNumber}
        onBankNameChange={(event) => setBankName(event.target.value)}
        bankName={bankName}
        onIbanChange={(event) => setIban(event.target.value)}
        iban={iban}
        onSubmitClick={() => {
          setPage(1);
          getAccountList(1, search, accountName, accountNumber, bankName, iban);
        }}
        onDeleteClick={deleteFilters}
      />

      <div className="mt-4">
        <Table
          heads={[
            "ردیف",
            "عنوان حساب",
            "شماره حساب",
            "شماره شبا",
            "نام بانک",
            "عملیات",
          ]}
          data={data?.map((item, index) => ({
            index: index + 1 + (page - 1) * 10,
            accountName: item.account_name,
            accountNumber: item.account_number,
            iban: item.iban,
            bankName: item.bank_name,
            operation: (
              <MoreButton
                accountId={item.id}
                onViewDetailsClick={() => {
                  setAccountId(item?.id);
                  setAccountDetailsModalOpen(true);
                }}
                onDeleteClick={() => {
                  setAccountId(item?.id);
                  setDeleteAccountModalType("delete");
                  setDeleteAccountModalOpen(true);
                }}
                onDeactiveClick={() => {
                  setAccountId(item?.id);
                  setDeleteAccountModalType("deactive");
                  setDeleteAccountModalOpen(true);
                }}
              />
            ),
          }))}
          loading={loading}
        />
      </div>

      <div className="mt-4 flex justify-end">
        <Pagination
          page={page}
          count={Math.ceil(count / 10)}
          onChange={pageHandler}
        />
      </div>

      <AccountDetailsModal
        open={accountDetailsModalOpen}
        onClose={() => setAccountDetailsModalOpen(false)}
        accountId={accountId}
      />

      <DeleteOrDeactiveAccountModal
        type={deleteAccountModalType}
        open={deleteAccountModalOpen}
        onClose={() => setDeleteAccountModalOpen(false)}
        accountId={accountId}
        onFinish={() => {
          setPage(1);
          getAccountList(1, search, accountName, accountNumber, bankName, iban);
        }}
      />
    </PanelBox>
  );
}
