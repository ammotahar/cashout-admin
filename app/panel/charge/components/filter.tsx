import { ChangeEventHandler, MouseEventHandler, ReactNode } from "react";
import { SelectChangeEvent } from "@mui/material";
import FilterContainer from "@/components/filterContainer";
import MaterialTextField from "@/components/materialTextField";
import Select from "@/components/select";
import Datepicker from "@/components/datepicker";
import {
  NOT_CONFIRMED,
  NOT_CONFIRMED_TITLE,
  CONFIRMED,
  CONFIRMED_TITLE,
  AWAITING_CONFIRMATION,
  AWAITING_CONFIRMATION_TITLE,
  SENDING_TO_IPG,
  SENDING_TO_IPG_TITLE,
  TRANSFER_TO_IPG_FAILED,
  TRANSFER_TO_IPG_FAILED_TITLE,
} from "@/enums/chargeBalanceStatusEnums";

export default function Filter({
  open,
  onClose,
  onProtalTrackNumberChange,
  portalTrackNumber,
  fromInsertDateChange,
  fromInsertDate,
  toInsertDateChange,
  toInsertDate,
  onFromIbanChange,
  fromIban,
  onToIbanChange,
  toIban,
  onChargeStatusChange,
  chargeStatus,
  onSubmitClick,
  onDeleteClick,
}: {
  open: boolean;
  onClose: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  onProtalTrackNumberChange?: ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  >;
  portalTrackNumber?: any;
  fromInsertDateChange?: (value: any) => void;
  fromInsertDate?: any;
  toInsertDateChange?: (value: any) => void;
  toInsertDate?: any;
  onFromIbanChange?: ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement>;
  fromIban?: any;
  onToIbanChange?: ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement>;
  toIban?: any;
  onChargeStatusChange?: (
    event: SelectChangeEvent<any>,
    child: ReactNode
  ) => void;
  chargeStatus?: any;
  onSubmitClick?: MouseEventHandler<HTMLButtonElement>;
  onDeleteClick?: MouseEventHandler<HTMLButtonElement>;
}) {
  return (
    <FilterContainer
      open={open}
      onClose={onClose}
      onSubmitClick={onSubmitClick}
      onDeleteClick={onDeleteClick}
    >
      <div className="grid grid-cols-1 lg:grid-cols-5 gap-4">
        <MaterialTextField
          label="کد پیگیری دریافت شده از درگاه"
          onChange={onProtalTrackNumberChange}
          value={portalTrackNumber}
          className="!col-span-1 !bg-white"
        />

        <Datepicker
          value={fromInsertDate}
          onChange={(value) =>
            fromInsertDateChange && fromInsertDateChange(value)
          }
          label="از تاریخ ثبت"
          containerClassNames="!col-span-1 !bg-white"
        />

        <Datepicker
          value={toInsertDate}
          onChange={(value) => toInsertDateChange && toInsertDateChange(value)}
          label="تا تاریخ ثبت"
          containerClassNames="!col-span-1 !bg-white"
        />

        {/* <Datepicker
          value={new Date()}
          // onChange={(value) => fromDateChange && fromDateChange(value)}
          label="از تاریخ تایید"
          containerClassNames="!col-span-1 !bg-white"
        />

        <Datepicker
          value={new Date()}
          // onChange={(value) => fromDateChange && fromDateChange(value)}
          label="تا تاریخ تایید"
          containerClassNames="!col-span-1 !bg-white"
        /> */}

        {/* <MaterialTextField
          label="کد پیگیری فیش بارگذاری شده"
          // onChange={onTrackNumberChange}
          // value={trackNumber}
          className="!col-span-1 !bg-white"
        /> */}

        <MaterialTextField
          label="شماره شبا مبدا"
          startAdornments="IR"
          onChange={onFromIbanChange}
          value={fromIban}
          className="!col-span-1 !bg-white"
        />

        <MaterialTextField
          label="شماره شبا مقصد"
          startAdornments="IR"
          onChange={onToIbanChange}
          value={toIban}
          className="!col-span-1 !bg-white"
        />

        <Select
          label="وضعیت"
          onChange={onChargeStatusChange}
          value={chargeStatus}
          items={[
            {
              id: NOT_CONFIRMED,
              name: NOT_CONFIRMED_TITLE,
            },
            {
              id: CONFIRMED,
              name: CONFIRMED_TITLE,
            },
            {
              id: AWAITING_CONFIRMATION,
              name: AWAITING_CONFIRMATION_TITLE,
            },
            {
              id: SENDING_TO_IPG,
              name: SENDING_TO_IPG_TITLE,
            },
            {
              id: TRANSFER_TO_IPG_FAILED,
              name: TRANSFER_TO_IPG_FAILED_TITLE,
            },
          ]}
          className="!col-span-1 !bg-white"
        />
      </div>
    </FilterContainer>
  );
}
