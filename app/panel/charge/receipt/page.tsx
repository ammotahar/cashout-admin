"use client";

import { useState, useEffect } from "react";
import Image from "next/image";
import { ExpandLess, ExpandMore } from "@mui/icons-material";
import PanelBox from "@/components/panelBox";
import SearchField from "@/components/searchField";
import Button from "@/components/button";
import Table from "@/components/table";
import Pagination from "@/components/pagination";
import ReceiptDetailsModal from "@/components/receiptDetailsModal";
import Filter from "../components/filter";
import {
  getChargeBalanceListApi,
  getChargeBalanceListXlsxApi,
} from "@/services/charge";
import { ChargeBalanceListType } from "@/services/charge/types";
import { rialSeparator } from "@/utils/rialSeperator";
import { jalaliConverter } from "@/utils/jalaliConverter";
import { getChargeBalanceStatusTitle } from "@/utils/getChargeBalanceStatusTitle";
import { getChargeBalanceTypeTitle } from "@/utils/getChargeBalanceTypeTitle";
import { MANUAL } from "@/enums/chargeBalanceTypeEnums";
import FilterIcon from "assets/icons/filter.svg";
import ExcelIcon from "assets/icons/excel.svg";

export default function ReceiptChargesPage() {
  const [search, setSearch] = useState("");
  const [filterOpen, setFilterOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<ChargeBalanceListType[]>([]);
  const [count, setCount] = useState(0);
  const [page, setPage] = useState(1);
  const [excelDownloadLoading, setExcelDownloadLoading] = useState(false);
  const [portalTrackNumber, setPortalTrackNumber] = useState("");
  const [fromInsertDate, setFromInsertDate] = useState<Date | undefined>();
  const [toInsertDate, setToInsertDate] = useState<Date | undefined>();
  const [fromIban, setFromIban] = useState("");
  const [toIban, setToIban] = useState("");
  const [chargeStatus, setChargeStatus] = useState<number | string>("");
  const [receiptDetailsModalOpen, setReceiptDetailsModalOpen] = useState(false);
  const [chargeId, setChargeId] = useState<number | undefined>();

  async function getChargeBalanceList(
    page?: number,
    search?: string,
    trackNumber?: number,
    fromInsertDate?: string,
    toInsertDate?: string,
    fromIban?: string,
    toIban?: string,
    status?: number
  ) {
    setLoading(true);

    const res = await getChargeBalanceListApi({
      per_page: 10,
      page_number: page,
      search,
      ...(trackNumber && { track_number: trackNumber }),
      from_date_insert_date: fromInsertDate,
      to_date_insert_date: toInsertDate,
      from_iban: fromIban && `IR${fromIban}`,
      to_iban: toIban && `IR${toIban}`,
      status,
      type: MANUAL,
    });

    if (res) {
      setData(res?.data?.data);
      setCount(res?.data?.count);
    }

    setLoading(false);
  }

  useEffect(() => {
    getChargeBalanceList(1, "", undefined, "", "", "", "", undefined);
  }, []);

  // useEffect(() => {
  //   setPage(1);
  //   getChargeBalanceList(
  //     1,
  //     search,
  //     parseInt(portalTrackNumber),
  //     typeof fromInsertDate !== "undefined" ? fromInsertDate.toISOString() : "",
  //     typeof toInsertDate !== "undefined" ? toInsertDate.toISOString() : "",
  //     fromIban,
  //     toIban,
  //     typeof chargeStatus !== "string" ? chargeStatus : undefined
  //   );
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [search]);

  function pageHandler(event: any, page: number) {
    setPage(page);
    getChargeBalanceList(
      page,
      search,
      parseInt(portalTrackNumber),
      typeof fromInsertDate !== "undefined" ? fromInsertDate.toISOString() : "",
      typeof toInsertDate !== "undefined" ? toInsertDate.toISOString() : "",
      fromIban,
      toIban,
      typeof chargeStatus !== "string" ? chargeStatus : undefined
    );
  }

  async function getChargeBalanceListXlsx(
    search?: string,
    trackNumber?: number,
    fromInsertDate?: string,
    toInsertDate?: string,
    fromIban?: string,
    toIban?: string,
    status?: number
  ) {
    setExcelDownloadLoading(true);

    const res = await getChargeBalanceListXlsxApi({
      search,
      ...(trackNumber && { track_number: trackNumber }),
      from_date_insert_date: fromInsertDate,
      to_date_insert_date: toInsertDate,
      from_iban: fromIban && `IR${fromIban}`,
      to_iban: toIban && `IR${toIban}`,
      status,
      type: MANUAL,
    });
    const blob = new Blob([res], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    });
    let url = window.URL.createObjectURL(blob);
    let a = document.createElement("a");
    a.href = url;
    a.download = "charge_report.xlsx";
    a.click();
    setExcelDownloadLoading(false);
  }

  function deleteFilters() {
    setPage(1);
    setPortalTrackNumber("");
    setFromInsertDate(undefined);
    setToInsertDate(undefined);
    setFromIban("");
    setToIban("");
    setChargeStatus("");
    getChargeBalanceList(1, search, undefined, "", "", "", "", undefined);
  }

  return (
    <PanelBox>
      <div className="w-full flex flex-col items-end gap-4 lg:flex-row lg:justify-between lg:items-center mb-4">
        <div className="flex items-center gap-4 w-full lg:w-1/2">
          {/* <SearchField
            placeholder="شبا مبدا"
            onChange={(value) => setSearch(value)}
          /> */}

          <Button
            className="!flex !items-center !justify-center lg:!justify-between lg:!px-2 !bg-orange !rounded-md !min-w-10 !min-h-10 lg:!w-32"
            variant="text"
            onClick={() => setFilterOpen(!filterOpen)}
          >
            <div className="flex items-center gap-1">
              <Image src={FilterIcon} alt="filter" />
              <span className="hidden lg:block text-xs text-white">فیلتر</span>
            </div>

            <div className="hidden lg:block">
              {filterOpen ? (
                <ExpandLess className="!text-xl !text-white" />
              ) : (
                <ExpandMore className="!text-xl !text-white" />
              )}
            </div>
          </Button>
        </div>

        <Button
          fullRounded={false}
          className="!col-span-1 !flex !items-center !justify-center !gap-2 !min-h-10 !bg-triatary !border !border-green !border-solid"
          padding="!px-2"
          onClick={() =>
            getChargeBalanceListXlsx(
              search,
              parseInt(portalTrackNumber),
              typeof fromInsertDate !== "undefined"
                ? fromInsertDate.toISOString()
                : "",
              typeof toInsertDate !== "undefined"
                ? toInsertDate.toISOString()
                : "",
              fromIban,
              toIban,
              typeof chargeStatus !== "string" ? chargeStatus : undefined
            )
          }
          loading={excelDownloadLoading}
        >
          <span className="text-xs text-black">خروجی اکسل</span>
          <Image src={ExcelIcon} alt="export" />
        </Button>
      </div>

      <Filter
        open={filterOpen}
        onClose={() => setFilterOpen(false)}
        onProtalTrackNumberChange={(event) =>
          setPortalTrackNumber(event.target.value)
        }
        portalTrackNumber={portalTrackNumber}
        fromInsertDateChange={(value) => setFromInsertDate(value)}
        fromInsertDate={fromInsertDate}
        toInsertDateChange={(value) => setToInsertDate(value)}
        toInsertDate={toInsertDate}
        onFromIbanChange={(event) => setFromIban(event.target.value)}
        fromIban={fromIban}
        onToIbanChange={(event) => setToIban(event.target.value)}
        toIban={toIban}
        onChargeStatusChange={(event) => setChargeStatus(event.target.value)}
        chargeStatus={chargeStatus}
        onSubmitClick={() => {
          setPage(1);
          getChargeBalanceList(
            1,
            search,
            parseInt(portalTrackNumber),
            typeof fromInsertDate !== "undefined"
              ? fromInsertDate.toISOString()
              : "",
            typeof toInsertDate !== "undefined"
              ? toInsertDate.toISOString()
              : "",
            fromIban,
            toIban,
            typeof chargeStatus !== "string" ? chargeStatus : undefined
          );
        }}
        onDeleteClick={deleteFilters}
      />

      <div className="mt-4">
        <Table
          heads={[
            "ردیف",
            "نام شرکت",
            "مبلغ",
            "تاریخ ثبت",
            "نام بانک",
            "شماره پیگیری",
            "تاریخ تایید",
            "شماره شبای مبدا",
            "شماره شبای مقصد",
            "نوع واریز",
            "وضعیت",
            "عملیات",
          ]}
          data={data?.map((item, index) => ({
            index: index + 1 + (page - 1) * 10,
            company: item?.company_name,
            amount: rialSeparator(item?.amount.toString()),
            insertDate: jalaliConverter(item?.insert_date),
            bankTitle: item?.bank_title || "-",
            trackNumber: item?.track_number,
            confirmDate: item?.confirm_date
              ? jalaliConverter(item?.confirm_date)
              : "-",
            fromIban: item?.from_iban,
            toIban: item?.to_iban,
            type: getChargeBalanceTypeTitle(item?.type),
            status: getChargeBalanceStatusTitle(item?.status),
            operation: (
              <Button
                variant="secondary"
                onClick={() => {
                  setChargeId(item?.id);
                  setReceiptDetailsModalOpen(true);
                }}
              >
                مشاهده جزییات
              </Button>
            ),
          }))}
          loading={loading}
        />
      </div>

      <div className="mt-4 flex justify-end">
        <Pagination
          page={page}
          count={Math.ceil(count / 10)}
          onChange={pageHandler}
        />
      </div>

      <ReceiptDetailsModal
        open={receiptDetailsModalOpen}
        onClose={() => setReceiptDetailsModalOpen(false)}
        chargeId={chargeId}
        onConfirmComplete={() => {
          setReceiptDetailsModalOpen(false);
          getChargeBalanceList(
            page,
            search,
            parseInt(portalTrackNumber),
            typeof fromInsertDate !== "undefined"
              ? fromInsertDate.toISOString()
              : "",
            typeof toInsertDate !== "undefined"
              ? toInsertDate.toISOString()
              : "",
            fromIban,
            toIban,
            typeof chargeStatus !== "string" ? chargeStatus : undefined
          );
        }}
      />
    </PanelBox>
  );
}
