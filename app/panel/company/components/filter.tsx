import { ChangeEventHandler, MouseEventHandler, ReactNode } from "react";
import { SelectChangeEvent } from "@mui/material";
import FilterContainer from "@/components/filterContainer";
import MaterialTextField from "@/components/materialTextField";
import Select from "@/components/select";

export default function Filter({
  open,
  onClose,
  onCodeChange,
  code,
  onCompanyStatusChange,
  companyStatus,
  onSubmitClick,
  onDeleteClick,
}: {
  open: boolean;
  onClose: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  onCodeChange?: ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement>;
  code?: any;
  onCompanyStatusChange?: (
    event: SelectChangeEvent<any>,
    child: ReactNode
  ) => void;
  companyStatus?: any;
  onSubmitClick?: MouseEventHandler<HTMLButtonElement>;
  onDeleteClick?: MouseEventHandler<HTMLButtonElement>;
}) {
  return (
    <FilterContainer
      open={open}
      onClose={onClose}
      onSubmitClick={onSubmitClick}
      onDeleteClick={onDeleteClick}
    >
      <div className="grid grid-cols-1 lg:grid-cols-5 gap-4">
        <MaterialTextField
          label="کد شرکت"
          onChange={onCodeChange}
          value={code}
          className="!col-span-1 !bg-white"
        />

        <Select
          label="وضعیت"
          onChange={onCompanyStatusChange}
          value={companyStatus}
          items={[
            {
              id: 1,
              name: "فعال",
            },
            {
              id: 2,
              name: "غیر فعال",
            },
          ]}
          className="!col-span-1 !bg-white"
        />
      </div>
    </FilterContainer>
  );
}
