"use client";

import { useState, useEffect, useCallback } from "react";
import { useSearchParams, useRouter } from "next/navigation";
import { useForm, SubmitHandler, Controller } from "react-hook-form";
import { toast } from "react-toastify";
import PanelBox from "@/components/panelBox";
import MaterialTextField from "@/components/materialTextField";
import Radio from "@/components/radio";
import Button from "@/components/button";
import {
  createCompanyApi,
  getCompanyDetailsApi,
  editCompanyApi,
} from "@/services/company";
import { CompanyDetailsType } from "@/services/company/types";

interface IFormInput {
  name: string;
  code: string;
  phone: string;
  min_deposit: string;
  number_create_user: string;
  status: boolean;
  description: string;
}

export default function CreateCompanyPage() {
  const [loading, setLoading] = useState(false);

  const router = useRouter();

  const searchParams = useSearchParams();

  const companyId = searchParams.get("id");

  const {
    handleSubmit,
    control,
    setValue,
    reset,
    formState: { errors },
  } = useForm<IFormInput>({
    defaultValues: {
      name: "",
      code: "",
      phone: "",
      min_deposit: "",
      number_create_user: "",
      status: true,
      description: "",
    },
  });

  const getCompanyDetails = useCallback(
    async (id: number) => {
      setLoading(true);

      const res = await getCompanyDetailsApi(id);

      if (res) {
        const data: CompanyDetailsType = res?.data;

        setValue("name", data.name);
        setValue("phone", data.phone);
        setValue("min_deposit", data.min_deposit?.toString());
        setValue("number_create_user", data.number_create_user?.toString());
        setValue("status", data.status);
        setValue("description", data.description);
        setValue("code", data.company_code);
      }

      setLoading(false);
    },
    [setValue]
  );

  useEffect(() => {
    if (companyId) {
      getCompanyDetails(parseInt(companyId));
    }
  }, [getCompanyDetails, companyId]);

  const onSubmit: SubmitHandler<IFormInput> = async (data) => {
    setLoading(true);

    if (companyId) {
      const res = await editCompanyApi(
        {
          name: data.name,
          code: parseInt(data.code),
          min_deposit: parseInt(data.min_deposit),
          status: data.status,
          phone: data.phone,
          number_create_user: parseInt(data.number_create_user),
          description: data.description,
        },
        parseInt(companyId)
      );

      if (res) {
        router.back();
        toast.success("شرکت با موفقیت ویرایش شد");
      }
    } else {
      const res = await createCompanyApi({
        name: data.name,
        code: parseInt(data.code),
        min_deposit: parseInt(data.min_deposit),
        status: data.status,
        ...(data.phone && { phone: data.phone }),
        ...(data.number_create_user && {
          number_create_user: parseInt(data.number_create_user),
        }),
        ...(data.phone && { phone: data.phone }),
        ...(data.description && { description: data.description }),
      });

      if (res) {
        reset();
        toast.success("شرکت با موفقیت ثبت شد");
      }
    }

    setLoading(false);
  };

  return (
    <PanelBox>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="grid grid-cols-1 gap-4 xl:grid-cols-3">
          <Controller
            name="name"
            control={control}
            rules={{
              required: "نام شرکت را وارد نمایید.",
            }}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="نام شرکت *"
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1"
                error={!!errors?.name}
                helperText={errors?.name?.message}
              />
            )}
          />

          <Controller
            name="code"
            control={control}
            rules={{
              required: "کد شرکت را وارد نمایید.",
              maxLength: 10,
            }}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="کد شرکت *"
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1"
                error={!!errors?.code}
                helperText={errors?.code?.message}
              />
            )}
          />

          <Controller
            name="phone"
            control={control}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="تلفن ثابت"
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1"
              />
            )}
          />

          <Controller
            name="min_deposit"
            control={control}
            rules={{
              required: "مبلغ بلوکه شرکت را وارد نمایید.",
            }}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="مبلغ بلوکه شرکت *"
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1"
                error={!!errors?.min_deposit}
                helperText={errors?.min_deposit?.message}
              />
            )}
          />

          <Controller
            name="number_create_user"
            control={control}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="مجوز تعریف تعداد کاربران شرکت"
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1"
              />
            )}
          />

          <Controller
            name="status"
            control={control}
            render={({ field: { onChange, value } }) => (
              <div className="xl:flex xl:items-center">
                <div>
                  <span>وضعیت</span>
                </div>

                <div className="flex items-center justify-center gap-8 bg-white rounded mt-2 xl:bg-transparent xl:mt-0 xl:mr-6">
                  <Radio
                    label="فعال"
                    checked={value === true}
                    handleChange={() => onChange(true)}
                  />
                  <Radio
                    label="غیر فعال"
                    checked={value === false}
                    handleChange={() => onChange(false)}
                  />
                </div>
              </div>
            )}
          />

          <Controller
            name="description"
            control={control}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="توضیحات"
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1 xl:!col-span-3"
                multiline
              />
            )}
          />
        </div>

        <div className="grid grid-cols-2 lg:grid-cols-8 gap-4 mt-8 w-full">
          <Button
            fullRounded={false}
            className="!col-span-1 lg:!col-start-7"
            type="submit"
            loading={loading}
          >
            {companyId ? "ویرایش" : "ثبت"}
          </Button>
          <Button
            variant="cancel"
            className="!col-span-1 lg:!col-start-8"
            loading={loading}
            onClick={() => (companyId ? router.back() : router.push("/panel"))}
          >
            انصراف
          </Button>
        </div>
      </form>
    </PanelBox>
  );
}
