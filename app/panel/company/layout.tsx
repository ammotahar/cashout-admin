import CompanyMenu from "./components/menu";

export default function UsersLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <div>
      <CompanyMenu />

      <div className="mt-4 flex flex-col items-center">{children}</div>
    </div>
  );
}
