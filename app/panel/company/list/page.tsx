"use client";

import { useState, useEffect } from "react";
import Image from "next/image";
import { ExpandLess, ExpandMore } from "@mui/icons-material";
import PanelBox from "@/components/panelBox";
import SearchField from "@/components/searchField";
import Button from "@/components/button";
import Table from "@/components/table";
import Pagination from "@/components/pagination";
import CompanyDetailsModal from "@/components/companyDetailsModal";
import Filter from "../components/filter";
import { getCompanyListApi, getCompanyListExcelApi } from "@/services/company";
import { CompanyListType } from "@/services/company/types";
import FilterIcon from "assets/icons/filter.svg";
import ExcelIcon from "assets/icons/excel.svg";

export default function CompanyListPage() {
  const [search, setSearch] = useState("");
  const [filterOpen, setFilterOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<CompanyListType[]>([]);
  const [count, setCount] = useState(0);
  const [page, setPage] = useState(1);
  const [excelDownloadLoading, setExcelDownloadLoading] = useState(false);
  const [code, setCode] = useState("");
  const [companyStatus, setCompanyStatus] = useState<number | string>("");
  const [companyDetailsModalOpen, setCompanyDetailsModalOpen] = useState(false);
  const [companyId, setCompanyId] = useState(0);

  async function getCompanyList(
    page?: number,
    search?: string,
    code?: string,
    status?: number
  ) {
    setLoading(true);

    const res = await getCompanyListApi({
      per_page: 10,
      page_number: page,
      search,
      code: code ? parseInt(code) : undefined,
      ...(status && { status: status === 1 ? true : false }),
    });

    if (res) {
      setData(res?.data?.data);
      setCount(res?.data?.count);
    }

    setLoading(false);
  }

  useEffect(() => {
    getCompanyList(1);
  }, []);

  // useEffect(() => {
  //   setPage(1);
  //   getCompanyList(
  //     1,
  //     search,
  //     code,
  //     typeof companyStatus !== "string" ? companyStatus : undefined
  //   );
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [search]);

  function pageHandler(event: any, page: number) {
    setPage(page);
    getCompanyList(
      page,
      search,
      code,
      typeof companyStatus !== "string" ? companyStatus : undefined
    );
  }

  async function getCompanyListExcel(
    search?: string,
    code?: string,
    status?: number
  ) {
    setExcelDownloadLoading(true);

    const res = await getCompanyListExcelApi({
      search,
      code: code ? parseInt(code) : undefined,
      ...(status && { status: status === 1 ? true : false }),
    });

    const blob = new Blob([res], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    });
    let url = window.URL.createObjectURL(blob);
    let a = document.createElement("a");
    a.href = url;
    a.download = "company_list.xlsx";
    a.click();
    setExcelDownloadLoading(false);
  }

  function deleteFilters() {
    setPage(1);
    setCode("");
    setCompanyStatus("");
    getCompanyList(1, search);
  }

  return (
    <PanelBox>
      <div className="w-full flex flex-col items-end gap-4 lg:flex-row lg:justify-between lg:items-center mb-4">
        <div className="flex items-center gap-4 w-full lg:w-1/2">
          {/* <SearchField
            placeholder="نام شرکت"
            onChange={(value) => setSearch(value)}
          /> */}

          <Button
            className="!flex !items-center !justify-center lg:!justify-between lg:!px-2 !bg-orange !rounded-md !min-w-10 !min-h-10 lg:!w-32"
            variant="text"
            onClick={() => setFilterOpen(!filterOpen)}
          >
            <div className="flex items-center gap-1">
              <Image src={FilterIcon} alt="filter" />
              <span className="hidden lg:block text-xs text-white">فیلتر</span>
            </div>

            <div className="hidden lg:block">
              {filterOpen ? (
                <ExpandLess className="!text-xl !text-white" />
              ) : (
                <ExpandMore className="!text-xl !text-white" />
              )}
            </div>
          </Button>
        </div>

        <Button
          fullRounded={false}
          className="!col-span-1 !flex !items-center !justify-center !gap-2 !min-h-10 !bg-triatary !border !border-green !border-solid"
          padding="!px-2"
          onClick={() =>
            getCompanyListExcel(
              search,
              code,
              typeof companyStatus !== "string" ? companyStatus : undefined
            )
          }
          loading={excelDownloadLoading}
        >
          <span className="text-xs text-black">خروجی اکسل</span>
          <Image src={ExcelIcon} alt="export" />
        </Button>
      </div>

      <Filter
        open={filterOpen}
        onClose={() => setFilterOpen(false)}
        onCodeChange={(event) => setCode(event.target.value)}
        code={code}
        onCompanyStatusChange={(event) => setCompanyStatus(event.target.value)}
        companyStatus={companyStatus}
        onSubmitClick={() => {
          setPage(1);
          getCompanyList(
            1,
            search,
            code,
            typeof companyStatus !== "string" ? companyStatus : undefined
          );
        }}
        onDeleteClick={deleteFilters}
      />

      <div className="mt-4">
        <Table
          heads={["ردیف", "نام شرکت", "کد شرکت", "وضعیت", "عملیات"]}
          data={data?.map((item, index) => ({
            index: index + 1 + (page - 1) * 10,
            name: item.name,
            code: item.code,
            status: item.status ? "فعال" : "غیر فعال",
            operation: (
              <Button
                variant="secondary"
                onClick={() => {
                  setCompanyId(item?.id);
                  setCompanyDetailsModalOpen(true);
                }}
              >
                مشاهده جزییات
              </Button>
            ),
          }))}
          loading={loading}
        />
      </div>

      <div className="mt-4 flex justify-end">
        <Pagination
          page={page}
          count={Math.ceil(count / 10)}
          onChange={pageHandler}
        />
      </div>

      <CompanyDetailsModal
        open={companyDetailsModalOpen}
        onClose={() => setCompanyDetailsModalOpen(false)}
        companyId={companyId}
      />
    </PanelBox>
  );
}
