import Header from "@/components/header";
import PanelContainer from "@/components/panelContainer";
import Sidebar from "@/components/sidebar";

export default function PanelLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <div className="flex">
      <Sidebar />

      <div className="w-full lg:w-full">
        <Header />
        <PanelContainer>{children}</PanelContainer>
      </div>
    </div>
  );
}
