import BalanceBox from "components/balanceBox";
import TransactionsBox from "components/transactionsBox";

export default function PanelPage() {
  return (
    <div className="flex flex-col lg:flex-row h-full gap-2">
      <BalanceBox />

      <TransactionsBox />
    </div>
  );
}
