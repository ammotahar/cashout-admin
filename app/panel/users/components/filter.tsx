import { ChangeEventHandler, MouseEventHandler, ReactNode } from "react";
import { SelectChangeEvent } from "@mui/material";
import FilterContainer from "@/components/filterContainer";
import MaterialTextField from "@/components/materialTextField";
import Select from "@/components/select";

export default function Filter({
  open,
  onClose,
  onMobileChange,
  mobile,
  onUsernameChange,
  username,
  onUserStatusChange,
  userStatus,
  onSubmitClick,
  onDeleteClick,
}: {
  open: boolean;
  onClose: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  onMobileChange?: ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement>;
  mobile?: any;
  onUsernameChange?: ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement>;
  username?: any;
  onUserStatusChange?: (
    event: SelectChangeEvent<any>,
    child: ReactNode
  ) => void;
  userStatus?: any;
  onSubmitClick?: MouseEventHandler<HTMLButtonElement>;
  onDeleteClick?: MouseEventHandler<HTMLButtonElement>;
}) {
  return (
    <FilterContainer
      open={open}
      onClose={onClose}
      onSubmitClick={onSubmitClick}
      onDeleteClick={onDeleteClick}
    >
      <div className="grid grid-cols-1 lg:grid-cols-5 gap-4">
        <MaterialTextField
          label="موبایل"
          onChange={onMobileChange}
          value={mobile}
          className="!col-span-1 !bg-white"
        />

        <MaterialTextField
          label="نام کاربری"
          onChange={onUsernameChange}
          value={username}
          className="!col-span-1 !bg-white"
        />

        <Select
          label="وضعیت"
          onChange={onUserStatusChange}
          value={userStatus}
          items={[
            {
              id: 1,
              name: "فعال",
            },
            {
              id: 2,
              name: "غیر فعال",
            },
          ]}
          className="!col-span-1 !bg-white"
        />
      </div>
    </FilterContainer>
  );
}
