"use client";

import { Controller } from "react-hook-form";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import MaterialTextField from "@/components/materialTextField";

export default function MaxAmount({
  expanded,
  handleChange,
  control,
  errors,
}: {
  expanded: string | false;
  handleChange: (
    panel: string
  ) => (event: React.SyntheticEvent, isExpanded: boolean) => void;
  control: any;
  errors: any;
}) {
  return (
    <Accordion
      expanded={expanded === "panel3"}
      onChange={handleChange("panel3")}
      className="!bg-form"
    >
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <span>تعیین سقف انتقال وجه</span>
      </AccordionSummary>
      <AccordionDetails>
        <div className="grid grid-cols-1 gap-4 xl:grid-cols-3">
          <Controller
            name="max_amount"
            control={control}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="سقف انتقال وجه داخلی"
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1"
                error={!!errors?.max_amount}
                helperText={errors?.max_amount?.message}
              />
            )}
          />

          <Controller
            name="max_amount_satna"
            control={control}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="سقف انتقال وجه ساتنا"
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1"
                error={!!errors?.max_amount_satna}
                helperText={errors?.max_amount_satna?.message}
              />
            )}
          />

          <Controller
            name="max_amount_paya"
            control={control}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="سقف انتقال وجه پایا"
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1"
                error={!!errors?.max_amount_paya}
                helperText={errors?.max_amount_paya?.message}
              />
            )}
          />
        </div>
      </AccordionDetails>
    </Accordion>
  );
}
