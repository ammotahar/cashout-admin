"use client";

import { Controller } from "react-hook-form";
import MaterialTextField from "@/components/materialTextField";
import Datepicker from "@/components/datepicker";
import Radio from "@/components/radio";
import FormTitle from "@/components/formTitle";

export default function PersonalInfo({
  control,
  errors,
}: {
  control: any;
  errors: any;
}) {
  return (
    <div>
      <FormTitle title="ورود اطلاعات شخصی" />

      <div className="grid grid-cols-1 xl:grid-cols-3 gap-4">
        <Controller
          name="first_name"
          control={control}
          rules={{
            required: "نام را وارد نمایید.",
          }}
          render={({ field: { onChange, value } }) => (
            <MaterialTextField
              label="نام *"
              value={value}
              onChange={onChange}
              className="!bg-white !col-span-1"
              error={!!errors?.first_name}
              helperText={errors?.first_name?.message}
            />
          )}
        />

        <Controller
          name="last_name"
          control={control}
          rules={{
            required: "نام خانوادگی را وارد نمایید.",
          }}
          render={({ field: { onChange, value } }) => (
            <MaterialTextField
              label="نام خانوادگی *"
              value={value}
              onChange={onChange}
              className="!bg-white !col-span-1"
              error={!!errors?.last_name}
              helperText={errors?.last_name?.message}
            />
          )}
        />

        <Controller
          name="birthdate"
          control={control}
          render={({ field: { onChange, value } }) => (
            <Datepicker
              label="تاریخ تولد"
              value={value}
              onChange={onChange}
              containerClassNames="!col-span-1 !bg-white"
            />
          )}
        />

        <Controller
          name="national_code"
          control={control}
          rules={{
            required: "کد ملی را وارد نمایید.",
          }}
          render={({ field: { onChange, value } }) => (
            <MaterialTextField
              label="کد ملی *"
              value={value}
              onChange={onChange}
              className="!bg-white !col-span-1"
              error={!!errors?.national_code}
              helperText={errors?.national_code?.message}
            />
          )}
        />

        <Controller
          name="mobile"
          control={control}
          rules={{
            required: "موبایل را وارد نمایید.",
          }}
          render={({ field: { onChange, value } }) => (
            <MaterialTextField
              label="موبایل *"
              value={value}
              onChange={onChange}
              className="!bg-white !col-span-1"
              error={!!errors?.mobile}
              helperText={errors?.mobile?.message}
            />
          )}
        />

        <Controller
          name="phone"
          control={control}
          render={({ field: { onChange, value } }) => (
            <MaterialTextField
              label="تلفن ثابت"
              value={value}
              onChange={onChange}
              className="!bg-white !col-span-1"
              error={!!errors?.phone}
              helperText={errors?.phone?.message}
            />
          )}
        />

        <Controller
          name="status"
          control={control}
          render={({ field: { onChange, value } }) => (
            <div className="xl:flex xl:items-center">
              <div>
                <span>وضعیت</span>
              </div>

              <div className="flex items-center justify-center gap-8 bg-white rounded mt-2 xl:bg-transparent xl:mt-0 xl:mr-6">
                <Radio
                  label="فعال"
                  checked={value === true}
                  handleChange={() => onChange(true)}
                />
                <Radio
                  label="غیر فعال"
                  checked={value === false}
                  handleChange={() => onChange(false)}
                />
              </div>
            </div>
          )}
        />
      </div>
    </div>
  );
}
