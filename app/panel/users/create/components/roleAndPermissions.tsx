"use client";

import { useState, useEffect, Fragment } from "react";
import { Controller } from "react-hook-form";
import Skeleton from "@mui/material/Skeleton";
import Delete from "@mui/icons-material/Delete";
import Select from "@/components/select";
import Button from "@/components/button";
import Radio from "@/components/radio";
import FormTitle from "@/components/formTitle";
import { getRolesListApi } from "@/services/access";
import { RolesListType } from "@/services/access/types";
import { getPermissionsListApi } from "@/services/access";
import { PermissionsListType } from "@/services/access/types";
import { getCompanyListApi } from "@/services/company";
import { CompanyListType } from "@/services/company/types";
import { getBankListApi } from "@/services/bank";
import { BankListType } from "@/services/bank/types";

export default function RoleAndPermissions({
  control,
  errors,
  setValue,
  getValues,
  watch,
}: {
  control: any;
  errors: any;
  setValue: any;
  getValues: any;
  watch: any;
}) {
  const [loading, setLoading] = useState(false);
  const [rolesList, setRolesList] = useState<RolesListType[]>([]);
  const [permissionsList, setPermissionsList] = useState<PermissionsListType[]>(
    []
  );
  const [selectedPermissions, setSelectedPermissions] = useState<string[]>([]);

  const [selected, setSelected] = useState<"company" | "bank">("company");
  const [companyList, setCompanyList] = useState<CompanyListType[]>([]);
  const [bankList, setBankList] = useState<BankListType[]>([]);

  useEffect(() => {
    if (getValues("permission_code_list").length === 0) {
      setSelectedPermissions([]);
    } else {
      setSelectedPermissions(getValues("permission_code_list"));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [watch("permission_code_list")]);

  async function getRolesList() {
    setLoading(true);

    const res = await getRolesListApi();

    if (res) {
      setRolesList(res?.data);
    }

    setLoading(false);
  }

  async function getPermissionsList() {
    setLoading(true);

    const res = await getPermissionsListApi();

    if (res) {
      setPermissionsList(res?.data);
    }

    setLoading(false);
  }

  async function getCompanyList() {
    setLoading(true);

    const res = await getCompanyListApi();

    if (res) {
      setCompanyList(res?.data?.data);
    }

    setLoading(false);
  }

  async function getBankList() {
    setLoading(true);

    const res = await getBankListApi();

    if (res) {
      setBankList(res?.data);
    }

    setLoading(false);
  }

  useEffect(() => {
    getCompanyList();
    getBankList();
    getRolesList();
    getPermissionsList();
  }, []);

  function handleSelectPermissions(
    permissionCode: string,
    onFormChange: (...event: any[]) => void
  ) {
    if (!selectedPermissions.includes(permissionCode)) {
      const _selectedPermissions = selectedPermissions.slice();

      _selectedPermissions.push(permissionCode);

      setSelectedPermissions(_selectedPermissions);

      onFormChange(_selectedPermissions);
    }
  }

  function handleDeletePermission(permissionCode: string) {
    const _selectedPermissions = selectedPermissions.slice();

    const index = _selectedPermissions.indexOf(permissionCode);

    if (index > -1) {
      // only splice array when item is found
      _selectedPermissions.splice(index, 1); // 2nd parameter means remove one item only
    }

    setSelectedPermissions(_selectedPermissions);

    setValue("permission_code_list", _selectedPermissions);
  }

  return (
    <div>
      <FormTitle title="تخصیص نقش و دسترسی" />

      <div className="grid grid-cols-1 gap-4 xl:grid-cols-3">
        {loading ? (
          <>
            <Skeleton variant="rounded" className="!col-span-1 !h-12" />

            <Skeleton variant="rounded" className="!col-span-1 !h-12" />
          </>
        ) : (
          <>
            <Controller
              name="role"
              control={control}
              rules={{
                required: "نقش را انتخاب نمایید.",
              }}
              render={({ field: { onChange, value } }) => (
                <Select
                  label="انتخاب نقش*"
                  onChange={onChange}
                  value={value}
                  items={rolesList.map((item) => {
                    return { id: item.id, name: item.title };
                  })}
                  className="!col-span-1 !bg-white"
                />
              )}
            />

            {watch("role") === 1 && (
              <Controller
                name="permission_code_list"
                control={control}
                rules={{
                  required: "دسترسی را انتخاب نمایید.",
                }}
                render={({ field: { onChange, value } }) => (
                  <Select
                    label="انتخاب دسترسی*"
                    value={value}
                    onChange={(event) =>
                      handleSelectPermissions(event.target.value, onChange)
                    }
                    items={permissionsList.map((item) => {
                      return { id: item.code, name: item.persian_title };
                    })}
                    className="!col-span-1 !bg-white"
                  />
                )}
              />
            )}

            {watch("role") === 2 && (
              <Controller
                name="company"
                control={control}
                render={({ field: { onChange, value } }) => (
                  <Select
                    label="انتخاب شرکت*"
                    onChange={onChange}
                    value={value}
                    items={companyList.map((item) => {
                      return {
                        id: item.id,
                        name: item.name,
                      };
                    })}
                    className="!w-3/4 !bg-white"
                  />
                )}
              />
            )}

            {/* <Controller
              name="bank"
              control={control}
              render={({ field: { onChange, value } }) => (
                <div className="flex items-center col-span-1 gap-6">
                  <Radio
                    label="بانک"
                    className="w-1/4"
                    checked={selected === "bank"}
                    handleChange={() => {
                      setSelected("bank");
                      setValue("company", null);
                    }}
                  />

                  <Select
                    label="انتخاب بانک*"
                    onChange={onChange}
                    value={value}
                    items={bankList.map((item) => {
                      return {
                        id: item.id,
                        name: item.name,
                      };
                    })}
                    className="!w-3/4 !bg-white"
                    disabled={selected !== "bank"}
                  />
                </div>
              )}
            /> */}
          </>
        )}
      </div>

      {watch("role") === 1 &&
        selectedPermissions.length > 0 &&
        permissionsList.length > 0 && (
          <div className="bg-white rounded mt-4 p-2">
            {selectedPermissions.map((item, index) => (
              <Fragment key={item}>
                <div className="flex items-center justify-between p-2">
                  <div>
                    <span>
                      {
                        permissionsList.filter(
                          (permission) => item === permission.code
                        )[0]?.persian_title
                      }
                    </span>
                  </div>

                  <Button
                    variant="icon"
                    onClick={() => handleDeletePermission(item)}
                  >
                    <Delete color="error" />
                  </Button>
                </div>

                {index + 1 !== selectedPermissions.length && (
                  <hr className="border-table" />
                )}
              </Fragment>
            ))}
          </div>
        )}
    </div>
  );
}
