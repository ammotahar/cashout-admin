"use client";

import { Controller } from "react-hook-form";
import MaterialTextField from "@/components/materialTextField";
import FormTitle from "@/components/formTitle";

export default function UserInfo({
  control,
  errors,
  isEditing = false,
}: {
  control: any;
  errors: any;
  isEditing?: boolean;
}) {
  return (
    <div>
      <FormTitle title="تعیین اطلاعات کاربری" />

      <div className="grid grid-cols-1 gap-4 xl:grid-cols-3">
        <Controller
          name="user_name"
          control={control}
          rules={{
            required: "نام کاربری را وارد نمایید.",
          }}
          render={({ field: { onChange, value } }) => (
            <MaterialTextField
              label="نام کاربری *"
              value={value}
              onChange={onChange}
              className="!bg-white !col-span-1"
              error={!!errors?.user_name}
              helperText={errors?.user_name?.message}
            />
          )}
        />

        <Controller
          name="password"
          control={control}
          rules={
            isEditing
              ? undefined
              : {
                  required: "کلمه عبور را وارد نمایید.",
                }
          }
          render={({ field: { onChange, value } }) => (
            <MaterialTextField
              label={isEditing ? "کلمه عبور" : "کلمه عبور *"}
              value={value}
              onChange={onChange}
              className="!bg-white !col-span-1"
              error={!!errors?.password}
              helperText={errors?.password?.message}
            />
          )}
        />
      </div>
    </div>
  );
}
