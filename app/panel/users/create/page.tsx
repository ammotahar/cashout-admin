"use client";

import { useState, useEffect, useCallback } from "react";
import { useSearchParams, useRouter } from "next/navigation";
import { useForm, SubmitHandler } from "react-hook-form";
import { toast } from "react-toastify";
import PanelBox from "@/components/panelBox";
import Button from "@/components/button";
import PersonalInfo from "./components/personalInfo";
import UserInfo from "./components/userInfo";
import MaxAmount from "./components/maxAmount";
import RoleAndPermissions from "./components/roleAndPermissions";
import {
  createUserApi,
  getUserDetailsApi,
  editUserApi,
} from "@/services/users";
import { UserDetailsType } from "@/services/users/types";

interface IFormInput {
  company: number | string;
  status: boolean;
  first_name: string;
  last_name: string;
  mobile: string;
  phone: string;
  birthdate: undefined | Date;
  national_code: string;
  user_name: string;
  password: string;
  bank: number | string;
  job_position: string;
  max_amount: string;
  max_amount_satna: string;
  max_amount_paya: string;
  role: number | string;
  permission_code_list: string[] | string;
}

export default function CreateUserPage() {
  const [loading, setLoading] = useState(false);

  const router = useRouter();

  const searchParams = useSearchParams();

  const userId = searchParams.get("id");

  const {
    handleSubmit,
    control,
    setValue,
    getValues,
    reset,
    watch,
    formState: { errors },
  } = useForm<IFormInput>({
    defaultValues: {
      company: "",
      status: true,
      first_name: "",
      last_name: "",
      mobile: "",
      phone: "",
      birthdate: undefined,
      national_code: "",
      user_name: "",
      password: "",
      bank: "",
      job_position: "",
      max_amount: "",
      max_amount_satna: "",
      max_amount_paya: "",
      role: "",
      permission_code_list: "",
    },
  });

  const getUserDetails = useCallback(
    async (id: number) => {
      setLoading(true);

      const res = await getUserDetailsApi(id);

      if (res) {
        const data: UserDetailsType = res?.data;

        setValue("company", data.company_id);
        setValue("status", data.status);
        setValue("first_name", data.first_name);
        setValue("last_name", data.last_name);
        setValue("mobile", data.mobile);
        setValue("phone", data.phone);
        setValue("birthdate", new Date(data.birthdate));
        setValue("national_code", data.national_code);
        setValue("user_name", data.user_name);
        setValue("bank", data.bank_id !== null ? data.bank_id : "");
        setValue("job_position", data.job_position);
        setValue("max_amount", data.max_amount !== null ? data.max_amount : "");
        setValue(
          "max_amount_satna",
          data.max_amount_satna !== null ? data.max_amount_satna : ""
        );
        setValue(
          "max_amount_paya",
          data.max_amount_paya !== null ? data.max_amount_paya : ""
        );
        setValue("role", data.role.id);
        data.role.id === 1 &&
          setValue(
            "permission_code_list",
            data.permission_code_list.map((item) => item.code)
          );
      }

      setLoading(false);
    },
    [setValue]
  );

  useEffect(() => {
    if (userId) {
      getUserDetails(parseInt(userId));
    }
  }, [getUserDetails, userId]);

  const onSubmit: SubmitHandler<IFormInput> = async (data) => {
    setLoading(true);

    if (userId) {
      const res = await editUserApi(
        {
          ...(typeof data.company !== "string" &&
            data.role == 2 && {
              company: data.company,
            }),
          status: data.status,
          first_name: data.first_name,
          last_name: data.last_name,
          mobile: data.mobile,
          ...(data.phone && { phone: data.phone }),
          ...(typeof data.birthdate === "object" && {
            birthdate: data.birthdate.toISOString().split("T")[0],
          }),
          national_code: data.national_code,
          user_name: data.user_name,
          ...(data.password.length > 0 && { password: data.password }),
          ...(typeof data.bank !== "string" && {
            bank: data.bank,
          }),
          ...(data.job_position && { job_position: data.job_position }),
          ...(data.max_amount && { max_amount: data.max_amount }),
          ...(data.max_amount_satna && {
            max_amount_satna: data.max_amount_satna,
          }),
          ...(data.max_amount_paya && {
            max_amount_paya: data.max_amount_paya,
          }),
          ...(typeof data.role !== "string" && {
            role: data.role,
          }),
          ...(typeof data.permission_code_list !== "string" &&
            data.role === 1 && {
              permission_code_list: data.permission_code_list,
            }),
        },
        parseInt(userId)
      );

      if (res) {
        router.back();
        toast.success("کاربر با موفقیت ویرایش شد");
      }
    } else {
      const res = await createUserApi({
        ...(typeof data.company !== "string" &&
          data.role == 2 && {
            company: data.company,
          }),
        status: data.status,
        first_name: data.first_name,
        last_name: data.last_name,
        mobile: data.mobile,
        ...(data.phone && { phone: data.phone }),
        ...(typeof data.birthdate === "object" && {
          birthdate: data.birthdate.toISOString().split("T")[0],
        }),
        national_code: data.national_code,
        user_name: data.user_name,
        password: data.password,
        ...(typeof data.bank !== "string" && {
          bank: data.bank,
        }),
        ...(data.job_position && { job_position: data.job_position }),
        ...(data.max_amount && { max_amount: data.max_amount }),
        ...(data.max_amount_satna && {
          max_amount_satna: data.max_amount_satna,
        }),
        ...(data.max_amount_paya && { max_amount_paya: data.max_amount_paya }),
        ...(typeof data.role !== "string" && {
          role: data.role,
        }),
        ...(typeof data.permission_code_list !== "string" &&
          data.role === 1 && {
            permission_code_list: data.permission_code_list,
          }),
      });

      if (res) {
        reset();
        toast.success("کاربر با موفقیت اضافه شد");
      }
    }

    setLoading(false);
  };

  return (
    <PanelBox>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="flex flex-col gap-6 lg:gap-10">
          <PersonalInfo control={control} errors={errors} />

          <UserInfo control={control} errors={errors} isEditing={!!userId} />

          {/* <MaxAmount
            expanded={expanded}
            handleChange={handleChange}
            control={control}
            errors={errors}
          /> */}

          <RoleAndPermissions
            control={control}
            errors={errors}
            setValue={setValue}
            getValues={getValues}
            watch={watch}
          />
        </div>

        <div className="grid grid-cols-2 lg:grid-cols-8 gap-4 mt-8 w-full">
          <Button
            fullRounded={false}
            className="!col-span-1 lg:!col-start-7"
            type="submit"
            loading={loading}
          >
            {userId ? "ویرایش" : "ثبت"}
          </Button>
          <Button
            variant="cancel"
            className="!col-span-1 lg:!col-start-8"
            loading={loading}
            onClick={() => (userId ? router.back() : router.push("/panel"))}
          >
            انصراف
          </Button>
        </div>
      </form>
    </PanelBox>
  );
}
