"use client";

import { useState, useEffect } from "react";
import Image from "next/image";
import { ExpandLess, ExpandMore } from "@mui/icons-material";
import PanelBox from "@/components/panelBox";
import SearchField from "@/components/searchField";
import Button from "@/components/button";
import Table from "@/components/table";
import Pagination from "@/components/pagination";
import UserDetailsModal from "@/components/userDetailsModal";
import Filter from "../components/filter";
import { getUsersListApi, getUsersListExcelApi } from "@/services/users";
import { UsersListType } from "@/services/users/types";
import FilterIcon from "assets/icons/filter.svg";
import ExcelIcon from "assets/icons/excel.svg";

export default function UsersListPage() {
  const [search, setSearch] = useState("");
  const [filterOpen, setFilterOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<UsersListType[]>([]);
  const [count, setCount] = useState(0);
  const [page, setPage] = useState(1);
  const [excelDownloadLoading, setExcelDownloadLoading] = useState(false);
  const [mobile, setMobile] = useState("");
  const [username, setUsername] = useState("");
  const [userStatus, setUserStatus] = useState<number | string>("");
  const [userDetailsModalOpen, setUserDetailsModalOpen] = useState(false);
  const [userId, setUserId] = useState(0);

  async function getUsersList(
    page?: number,
    search?: string,
    mobile?: string,
    username?: string,
    status?: number
  ) {
    setLoading(true);

    const res = await getUsersListApi({
      per_page: 10,
      page_number: page,
      search,
      mobile,
      user_name: username,
      ...(status && { status: status === 1 ? true : false }),
    });

    if (res) {
      setData(res?.data?.data);
      setCount(res?.data?.count);
    }

    setLoading(false);
  }

  useEffect(() => {
    getUsersList(1);
  }, []);

  // useEffect(() => {
  //   setPage(1);
  //   getUsersList(
  //     1,
  //     search,
  //     mobile,
  //     username,
  //     typeof userStatus !== "string" ? userStatus : undefined
  //   );
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [search]);

  function pageHandler(event: any, page: number) {
    setPage(page);
    getUsersList(
      page,
      search,
      mobile,
      username,
      typeof userStatus !== "string" ? userStatus : undefined
    );
  }

  async function getUsersListExcel(
    search?: string,
    mobile?: string,
    username?: string,
    status?: number
  ) {
    setExcelDownloadLoading(true);

    const res = await getUsersListExcelApi({
      search,
      mobile,
      user_name: username,
      ...(status && { status: status === 1 ? true : false }),
    });
    const blob = new Blob([res], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    });
    let url = window.URL.createObjectURL(blob);
    let a = document.createElement("a");
    a.href = url;
    a.download = "users_list.xlsx";
    a.click();
    setExcelDownloadLoading(false);
  }

  function deleteFilters() {
    setPage(1);
    setMobile("");
    setUsername("");
    setUserStatus("");
    getUsersList(1, search);
  }

  return (
    <PanelBox>
      <div className="w-full flex flex-col items-end gap-4 lg:flex-row lg:justify-between lg:items-center mb-4">
        <div className="flex items-center gap-4 w-full lg:w-1/2">
          {/* <SearchField
            placeholder="کد ملی"
            onChange={(value) => setSearch(value)}
          /> */}

          <Button
            className="!flex !items-center !justify-center lg:!justify-between lg:!px-2 !bg-orange !rounded-md !min-w-10 !min-h-10 lg:!w-32"
            variant="text"
            onClick={() => setFilterOpen(!filterOpen)}
          >
            <div className="flex items-center gap-1">
              <Image src={FilterIcon} alt="filter" />
              <span className="hidden lg:block text-xs text-white">فیلتر</span>
            </div>

            <div className="hidden lg:block">
              {filterOpen ? (
                <ExpandLess className="!text-xl !text-white" />
              ) : (
                <ExpandMore className="!text-xl !text-white" />
              )}
            </div>
          </Button>
        </div>

        <Button
          fullRounded={false}
          className="!col-span-1 !flex !items-center !justify-center !gap-2 !min-h-10 !bg-triatary !border !border-green !border-solid"
          padding="!px-2"
          onClick={() =>
            getUsersListExcel(
              search,
              mobile,
              username,
              typeof userStatus !== "string" ? userStatus : undefined
            )
          }
          loading={excelDownloadLoading}
        >
          <span className="text-xs text-black">خروجی اکسل</span>
          <Image src={ExcelIcon} alt="export" />
        </Button>
      </div>

      <Filter
        open={filterOpen}
        onClose={() => setFilterOpen(false)}
        onMobileChange={(event) => setMobile(event.target.value)}
        mobile={mobile}
        onUsernameChange={(event) => setUsername(event.target.value)}
        username={username}
        onUserStatusChange={(event) => setUserStatus(event.target.value)}
        userStatus={userStatus}
        onSubmitClick={() => {
          setPage(1);
          getUsersList(
            1,
            search,
            mobile,
            username,
            typeof userStatus !== "string" ? userStatus : undefined
          );
        }}
        onDeleteClick={deleteFilters}
      />

      <div className="mt-4">
        <Table
          heads={[
            "ردیف",
            "نام و نام خانوادگی",
            "کد ملی",
            "شماره موبایل",
            "نام کاربری",
            "وضعیت",
            "عملیات",
          ]}
          data={data?.map((item, index) => ({
            index: index + 1 + (page - 1) * 10,
            name: `${item.first_name} ${item.last_name}`,
            nationalCode: item.national_code,
            mobile: item.mobile,
            username: item.user_name,
            status: item.status ? "فعال" : "غیر فعال",
            operation: (
              <Button
                variant="secondary"
                onClick={() => {
                  setUserId(item?.id);
                  setUserDetailsModalOpen(true);
                }}
              >
                مشاهده جزییات
              </Button>
            ),
          }))}
          loading={loading}
        />
      </div>

      <div className="mt-4 flex justify-end">
        <Pagination
          page={page}
          count={Math.ceil(count / 10)}
          onChange={pageHandler}
        />
      </div>

      <UserDetailsModal
        open={userDetailsModalOpen}
        onClose={() => setUserDetailsModalOpen(false)}
        userId={userId}
      />
    </PanelBox>
  );
}
