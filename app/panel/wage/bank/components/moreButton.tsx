"use client";

import { useState } from "react";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Button from "@/components/button";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";

export default function MoreButton({
  onViewDetailsClick,
  showDeactiveButton,
  onDeactiveClick,
}: {
  onViewDetailsClick?: () => void;
  showDeactiveButton: boolean;
  onDeactiveClick?: () => void;
}) {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const open = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <Button variant="icon" onClick={handleClick}>
        <MoreHorizIcon />
      </Button>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          "aria-labelledby": "basic-button",
        }}
      >
        <MenuItem
          onClick={() => {
            onViewDetailsClick && onViewDetailsClick();
            handleClose();
          }}
          className="!text-primary"
        >
          مشاهده جزئیات
        </MenuItem>

        {showDeactiveButton && (
          <MenuItem
            onClick={() => {
              onDeactiveClick && onDeactiveClick();
              handleClose();
            }}
            className="!text-error"
          >
            غیر فعال کردن
          </MenuItem>
        )}
      </Menu>
    </>
  );
}
