"use client";

import { useState } from "react";
import { useRouter } from "next/navigation";
import { useForm, SubmitHandler, Controller } from "react-hook-form";
import { toast } from "react-toastify";
import PanelBox from "@/components/panelBox";
import FormTitle from "@/components/formTitle";
import MaterialTextField from "@/components/materialTextField";
import Select from "@/components/select";
import Button from "@/components/button";
import { createBankWageApi } from "@/services/wage";
import {
  CONSTANT,
  CONSTANT_TITLE,
  PERCENT,
  PERCENT_TITLE,
} from "@/enums/wageTypeEnums";
import {
  SATNA,
  SATNA_TITLE,
  PAYA,
  PAYA_TITLE,
  INTERNAL,
  INTERNAL_TITLE,
  INQUIRY_IBAN,
  INQUIRY_IBAN_TITLE,
} from "@/enums/transactionTypeEnums";
import { PER_AMOUNT, PER_AMOUNT_TITLE } from "@/enums/restrictionTypeEnums";

interface IFormInput {
  wage_type: 0 | 1 | string;
  wage_amount: string;
  transaction_type: 0 | 1 | 2 | string;
  is_active?: boolean;
  max_wage?: string;
  min_wage?: string;
  from_amount?: string;
  to_amount?: string;
  description?: string;
  restriction_type?: 0 | 1 | string;
  restriction_value?: string;
  extra_wage_amount?: string;
}

export default function CreateEniacWagePage() {
  const [loading, setLoading] = useState(false);

  const router = useRouter();

  const {
    handleSubmit,
    control,
    watch,
    reset,
    formState: { errors },
  } = useForm<IFormInput>({
    defaultValues: {
      wage_type: "",
      wage_amount: "",
      transaction_type: "",
      is_active: true,
      max_wage: "",
      min_wage: "",
      from_amount: "",
      to_amount: "",
      description: "",
      restriction_type: "",
      restriction_value: "",
      extra_wage_amount: "",
    },
  });

  const onSubmit: SubmitHandler<IFormInput> = async (data) => {
    setLoading(true);

    const res = await createBankWageApi({
      wage_type: typeof data.wage_type !== "string" ? data.wage_type : 0,
      transaction_type:
        typeof data.transaction_type !== "string" ? data.transaction_type : 0,
      wage_amount: data.wage_amount,
      ...(data.max_wage && { max_wage: parseInt(data.max_wage) }),
      ...(data.min_wage && { min_wage: parseInt(data.min_wage) }),
      ...(data.from_amount && { from_amount: parseInt(data.from_amount) }),
      ...(data.to_amount && { to_amount: parseInt(data.to_amount) }),
      ...(typeof data.transaction_type !== "string" && {
        transaction_type: data.transaction_type,
      }),
      ...(data.restriction_value && {
        restriction_value: parseInt(data.restriction_value),
      }),
      ...(data.extra_wage_amount && {
        extra_wage_amount: parseInt(data.extra_wage_amount),
      }),
    });

    if (res) {
      reset();
      toast.success("کارمزد بانکی با موفقیت ثبت شد");
    }

    setLoading(false);
  };

  function getAmountInputLabel() {
    switch (watch("wage_type")) {
      case CONSTANT:
        return "مبلغ کارمزد*";

      case PERCENT:
        return "درصد کارمزد*";

      default:
        return "مبلغ کارمزد/درصد کارمزد*";
    }
  }

  return (
    <PanelBox>
      <FormTitle title="تعریف کارمزد بانکی" />

      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="grid grid-cols-1 gap-4 xl:grid-cols-3">
          <Controller
            name="wage_type"
            control={control}
            rules={{
              required: "نوع کارمزد را انتخاب نمایید.",
            }}
            render={({ field: { onChange, value } }) => (
              <Select
                label="نوع کارمزد *"
                className="!col-span-1"
                onChange={onChange}
                value={value}
                items={[
                  {
                    id: CONSTANT,
                    name: CONSTANT_TITLE,
                  },
                  {
                    id: PERCENT,
                    name: PERCENT_TITLE,
                  },
                ]}
                error={!!errors?.wage_type}
                helperText={errors?.wage_type?.message}
              />
            )}
          />

          <Controller
            name="transaction_type"
            control={control}
            rules={{
              required: "نوع تراکنش را انتخاب نمایید.",
            }}
            render={({ field: { onChange, value } }) => (
              <Select
                label="نوع تراکنش *"
                className="!col-span-1"
                onChange={onChange}
                value={value}
                items={[
                  {
                    id: SATNA,
                    name: SATNA_TITLE,
                  },
                  {
                    id: PAYA,
                    name: PAYA_TITLE,
                  },
                  // {
                  //   id: INTERNAL,
                  //   name: INTERNAL_TITLE,
                  // },
                  // {
                  //   id: INQUIRY_IBAN,
                  //   name: INQUIRY_IBAN_TITLE,
                  // },
                ]}
                error={!!errors?.transaction_type}
                helperText={errors?.transaction_type?.message}
              />
            )}
          />

          <Controller
            name="restriction_type"
            control={control}
            render={({ field: { onChange, value } }) => (
              <Select
                label="نوع محدودیت"
                className="!col-span-1"
                onChange={onChange}
                value={value}
                items={[
                  {
                    id: PER_AMOUNT,
                    name: PER_AMOUNT_TITLE,
                  },
                ]}
              />
            )}
          />

          <Controller
            name="from_amount"
            control={control}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="از مبلغ"
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1"
                startAdornments="ریال"
                isFormatNumber
              />
            )}
          />

          <Controller
            name="to_amount"
            control={control}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="تا مبلغ"
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1"
                startAdornments="ریال"
                isFormatNumber
              />
            )}
          />

          <Controller
            name="min_wage"
            control={control}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="کف کارمزد"
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1 !col-start-1"
                startAdornments="ریال"
                isFormatNumber
              />
            )}
          />

          <Controller
            name="max_wage"
            control={control}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="سقف کارمزد"
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1"
                startAdornments="ریال"
                isFormatNumber
              />
            )}
          />

          <Controller
            name="wage_amount"
            control={control}
            rules={{
              required: "مبلغ کارمزد/درصد کارمزد را وارد نمایید.",
            }}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label={getAmountInputLabel()}
                value={value}
                onChange={onChange}
                className="!bg-white !col-span-1"
                startAdornments={
                  watch("wage_type") === CONSTANT ? "ریال" : undefined
                }
                isFormatNumber={watch("wage_type") === CONSTANT}
                error={!!errors?.wage_amount}
                helperText={errors?.wage_amount?.message}
              />
            )}
          />

          {watch("restriction_type") === PER_AMOUNT && (
            <>
              <Controller
                name="restriction_value"
                control={control}
                rules={{
                  required: "مقدار محدودیت را وارد نمایید.",
                }}
                render={({ field: { onChange, value } }) => (
                  <MaterialTextField
                    label="مقدار محدودیت*"
                    value={value}
                    onChange={onChange}
                    className="!bg-white !col-span-1"
                    startAdornments="ریال"
                    isFormatNumber
                    error={!!errors?.restriction_value}
                    helperText={errors?.restriction_value?.message}
                  />
                )}
              />

              <Controller
                name="extra_wage_amount"
                control={control}
                rules={{
                  required: "مبلغ مازاد کارمزد را وارد نمایید.",
                }}
                render={({ field: { onChange, value } }) => (
                  <MaterialTextField
                    label="مبلغ مازاد کارمزد*"
                    value={value}
                    onChange={onChange}
                    className="!bg-white !col-span-1"
                    startAdornments="ریال"
                    isFormatNumber
                    error={!!errors?.extra_wage_amount}
                    helperText={errors?.extra_wage_amount?.message}
                  />
                )}
              />
            </>
          )}
        </div>

        <div className="grid grid-cols-2 lg:grid-cols-8 gap-4 mt-8 w-full">
          <Button
            fullRounded={false}
            className="!col-span-1 lg:!col-start-7"
            type="submit"
            loading={loading}
          >
            ثبت
          </Button>
          <Button
            variant="cancel"
            className="!col-span-1 lg:!col-start-8"
            loading={loading}
            onClick={() => router.push("/panel/wage/bank/list")}
          >
            انصراف
          </Button>
        </div>
      </form>
    </PanelBox>
  );
}
