"use client";

import { useState, useEffect } from "react";
import Link from "next/link";
import Image from "next/image";
import { ExpandLess, ExpandMore } from "@mui/icons-material";
import PanelBox from "@/components/panelBox";
import Button from "@/components/button";
import Table from "@/components/table";
import Pagination from "@/components/pagination";
import WageDetailsModal from "@/components/wageDetailsModal";
import Filter from "../components/filter";
import MoreButton from "../components/moreButton";
import DeactiveWageModal from "@/components/deactiveWageModal";
import { miladiToJalali } from "@/utils/jalaliConverter";
import { rialSeparator } from "@/utils/rialSeperator";
import { getBankWageListApi, getBankWageListXlsxApi } from "@/services/wage";
import { BankWageListType } from "@/services/wage/types";
import FilterIcon from "assets/icons/filter.svg";
import ExcelIcon from "assets/icons/excel.svg";
import { CONSTANT } from "@/enums/wageTypeEnums";

export default function EniacWageListPage() {
  const [filterOpen, setFilterOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<BankWageListType[]>([]);
  const [count, setCount] = useState(0);
  const [page, setPage] = useState(1);
  const [excelDownloadLoading, setExcelDownloadLoading] = useState(false);
  const [wageAmount, setWageAmount] = useState("");
  const [createTime, setCreateTime] = useState<Date | null>(null);
  const [isActive, setIsActive] = useState<number | string>("");
  const [wageType, setWageType] = useState<number | string>("");
  const [transactionType, setTransactionType] = useState<number | string>("");
  const [wageDetailsModalOpen, setWageDetailsModalOpen] = useState(false);
  const [deactiveWageModalOpen, setDeactiveWageModalOpen] = useState(false);
  const [wageId, setWageId] = useState(0);

  async function getBankWageList(
    page?: number,
    wage_amount?: number,
    create_time?: string,
    is_active?: boolean,
    transaction_type?: string,
    wage_type?: string
  ) {
    setLoading(true);

    const res = await getBankWageListApi({
      per_page: 10,
      page_number: page,
      ...(wage_amount && { wage_amount }),
      create_time: create_time && create_time.split("T")[0],
      is_active,
      transaction_type,
      wage_type,
    });

    if (res) {
      setData(res?.data?.data);
      setCount(res?.data?.count);
    }

    setLoading(false);
  }

  useEffect(() => {
    getBankWageList(1);
  }, []);

  function pageHandler(event: any, page: number) {
    setPage(page);
    getBankWageList(
      page,
      parseInt(wageAmount),
      createTime !== null ? createTime.toISOString() : "",
      typeof isActive !== "string"
        ? isActive === 1
          ? true
          : false
        : undefined,
      transactionType.toString(),
      wageType.toString()
    );
  }

  async function getBankWageListXlsx(
    wage_amount?: number,
    create_time?: string,
    is_active?: boolean,
    transaction_type?: string,
    wage_type?: string
  ) {
    setExcelDownloadLoading(true);

    const res = await getBankWageListXlsxApi({
      ...(wage_amount && { wage_amount }),
      create_time: create_time && create_time.split("T")[0],
      is_active,
      transaction_type,
      wage_type,
    });

    const blob = new Blob([res], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    });
    let url = window.URL.createObjectURL(blob);
    let a = document.createElement("a");
    a.href = url;
    a.download = "bank_wage_list.xlsx";
    a.click();
    setExcelDownloadLoading(false);
  }

  function deleteFilters() {
    setPage(1);
    setWageAmount("");
    setCreateTime(null);
    setIsActive("");
    setWageType("");
    setTransactionType("");
    getBankWageList(1);
  }

  return (
    <PanelBox>
      <Link href="/panel/wage/bank/create">
        <Button fullRounded={false} className="!mb-4">
          تعریف کارمزد بانکی
        </Button>
      </Link>

      <div className="w-full flex flex-col items-end gap-4 lg:flex-row lg:justify-between lg:items-center mb-4">
        <div className="flex items-center gap-4 w-full lg:w-1/2">
          <Button
            className="!flex !items-center !justify-center lg:!justify-between lg:!px-2 !bg-orange !rounded-md !min-w-10 !min-h-10 lg:!w-32"
            variant="text"
            onClick={() => setFilterOpen(!filterOpen)}
          >
            <div className="flex items-center gap-1">
              <Image src={FilterIcon} alt="filter" />
              <span className="hidden lg:block text-xs text-white">فیلتر</span>
            </div>

            <div className="hidden lg:block">
              {filterOpen ? (
                <ExpandLess className="!text-xl !text-white" />
              ) : (
                <ExpandMore className="!text-xl !text-white" />
              )}
            </div>
          </Button>
        </div>

        <Button
          fullRounded={false}
          className="!col-span-1 !flex !items-center !justify-center !gap-2 !min-h-10 !bg-triatary !border !border-green !border-solid"
          padding="!px-2"
          onClick={() =>
            getBankWageListXlsx(
              parseInt(wageAmount),
              createTime !== null ? createTime.toISOString() : "",
              typeof isActive !== "string"
                ? isActive === 1
                  ? true
                  : false
                : undefined,
              transactionType.toString(),
              wageType.toString()
            )
          }
          loading={excelDownloadLoading}
        >
          <span className="text-xs text-black">خروجی اکسل</span>
          <Image src={ExcelIcon} alt="export" />
        </Button>
      </div>

      <Filter
        open={filterOpen}
        onClose={() => setFilterOpen(false)}
        onAmountChange={(event) => setWageAmount(event.target.value)}
        amount={wageAmount}
        onCreateTimeChange={(value) => setCreateTime(value)}
        createTime={createTime}
        onIsActiveChange={(event) => setIsActive(event.target.value)}
        isActive={isActive}
        onWageTypeChange={(event) => setWageType(event.target.value)}
        wageType={wageType}
        onTransactionTypeChange={(event) =>
          setTransactionType(event.target.value)
        }
        transactionType={transactionType}
        onSubmitClick={() => {
          setPage(1);
          getBankWageList(
            1,
            parseInt(wageAmount),
            createTime !== null ? createTime.toISOString() : "",
            typeof isActive !== "string"
              ? isActive === 1
                ? true
                : false
              : undefined,
            transactionType.toString(),
            wageType.toString()
          );
        }}
        onDeleteClick={deleteFilters}
      />

      <div className="mt-4">
        <Table
          heads={[
            "ردیف",
            "نوع کارمزد",
            "نوع تراکنش",
            "مبلغ",
            "تاریخ ثبت",
            "وضعیت",
            "عملیات",
          ]}
          data={data?.map((item, index) => ({
            index: index + 1 + (page - 1) * 10,
            wageType: item.wage_type_title,
            transactionType: item.transaction_type_title,
            wageAmount:
              item.wage_type === CONSTANT
                ? rialSeparator(item.wage_amount.toString())
                : item.wage_amount,
            createTime: miladiToJalali(item.create_time),
            isActive: item.is_active ? "فعال" : "غیر فعال",
            operation: (
              <MoreButton
                onViewDetailsClick={() => {
                  setWageId(item?.id);
                  setWageDetailsModalOpen(true);
                }}
                showDeactiveButton={item.is_active}
                onDeactiveClick={() => {
                  setWageId(item?.id);
                  setDeactiveWageModalOpen(true);
                }}
              />
            ),
          }))}
          loading={loading}
        />
      </div>

      <div className="mt-4 flex justify-end">
        <Pagination
          page={page}
          count={Math.ceil(count / 10)}
          onChange={pageHandler}
        />
      </div>

      <WageDetailsModal
        open={wageDetailsModalOpen}
        onClose={() => setWageDetailsModalOpen(false)}
        wageId={wageId}
        type="bank"
      />

      <DeactiveWageModal
        open={deactiveWageModalOpen}
        onClose={() => setDeactiveWageModalOpen(false)}
        wageId={wageId}
        type="bank"
        onFinish={() => {
          setDeactiveWageModalOpen(false);
          setPage(1);
          getBankWageList(
            1,
            parseInt(wageAmount),
            createTime !== null ? createTime.toISOString() : "",
            typeof isActive !== "string"
              ? isActive === 1
                ? true
                : false
              : undefined,
            transactionType.toString(),
            wageType.toString()
          );
        }}
      />
    </PanelBox>
  );
}
