"use client";

import { usePathname } from "next/navigation";
import Link from "next/link";
import Image from "next/image";
import SingleCheckoutIcon from "assets/icons/single-checkout.svg";
import GroupCheckoutIcon from "assets/icons/group-checkout.svg";

export default function WageMenu() {
  const pathname = usePathname();

  return (
    <div className="flex items-center justify-between gap-4 overflow-auto">
      <Link
        href="/panel/wage/eniac/list"
        className={`flex items-center justify-center bg-white rounded-lg w-1/2 min-w-48 h-9 gap-2 border ${
          pathname.startsWith("/panel/wage/eniac")
            ? "border-primary"
            : "border-gray"
        }`}
      >
        <Image
          src={SingleCheckoutIcon}
          alt="single-checkout"
          className="w-4 h-4"
        />
        <span
          className={`text-xs ${
            pathname.startsWith("/panel/wage/eniac")
              ? "text-primary"
              : "text-gray"
          }`}
        >
          کارمزد انیاکی
        </span>
      </Link>

      <Link
        href="/panel/wage/bank/list"
        className={`flex items-center justify-center bg-white rounded-lg w-1/2 min-w-48 h-9 gap-2 border ${
          pathname.startsWith("/panel/wage/bank")
            ? "border-primary"
            : "border-gray"
        }`}
      >
        <Image
          src={GroupCheckoutIcon}
          alt="group-checkout"
          className="w-4 h-4"
        />
        <span
          className={`text-xs ${
            pathname.startsWith("/panel/wage/bank")
              ? "text-primary"
              : "text-gray"
          }`}
        >
          کارمزد بانکی
        </span>
      </Link>
    </div>
  );
}
