import { ChangeEventHandler, MouseEventHandler, ReactNode } from "react";
import { SelectChangeEvent } from "@mui/material";
import FilterContainer from "@/components/filterContainer";
import MaterialTextField from "@/components/materialTextField";
import Select from "@/components/select";
import Datepicker from "@/components/datepicker";
import {
  CONSTANT,
  CONSTANT_TITLE,
  PERCENT,
  PERCENT_TITLE,
  LEVEL,
  LEVEL_TITLE,
} from "@/enums/wageTypeEnums";
import {
  SATNA,
  SATNA_TITLE,
  PAYA,
  PAYA_TITLE,
  INTERNAL,
  INTERNAL_TITLE,
  INQUIRY_IBAN,
  INQUIRY_IBAN_TITLE,
} from "@/enums/transactionTypeEnums";

export default function Filter({
  open,
  onClose,
  onAmountChange,
  amount,
  onCreateTimeChange,
  createTime,
  onIsActiveChange,
  isActive,
  onWageTypeChange,
  wageType,
  onTransactionTypeChange,
  transactionType,
  onSubmitClick,
  onDeleteClick,
}: {
  open: boolean;
  onClose: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  onAmountChange?: ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement>;
  amount?: any;
  onCreateTimeChange?: (value: any) => void;
  createTime?: any;
  onIsActiveChange?: (event: SelectChangeEvent<any>, child: ReactNode) => void;
  isActive?: any;
  onWageTypeChange?: (event: SelectChangeEvent<any>, child: ReactNode) => void;
  wageType?: any;
  onTransactionTypeChange?: (
    event: SelectChangeEvent<any>,
    child: ReactNode
  ) => void;
  transactionType?: any;
  onSubmitClick?: MouseEventHandler<HTMLButtonElement>;
  onDeleteClick?: MouseEventHandler<HTMLButtonElement>;
}) {
  return (
    <FilterContainer
      open={open}
      onClose={onClose}
      onSubmitClick={onSubmitClick}
      onDeleteClick={onDeleteClick}
    >
      <div className="grid grid-cols-1 lg:grid-cols-5 gap-4">
        <MaterialTextField
          label="مبلغ"
          onChange={onAmountChange}
          value={amount}
          className="!col-span-1 !bg-white"
          isFormatNumber
        />

        <Datepicker
          value={createTime}
          onChange={(value) => onCreateTimeChange && onCreateTimeChange(value)}
          label="تاریخ ثبت"
          containerClassNames="!col-span-1 !bg-white"
        />

        <Select
          label="نوع کارمزد"
          onChange={onWageTypeChange}
          value={wageType}
          items={[
            {
              id: CONSTANT,
              name: CONSTANT_TITLE,
            },
            {
              id: PERCENT,
              name: PERCENT_TITLE,
            },
            {
              id: LEVEL,
              name: LEVEL_TITLE,
            },
          ]}
          className="!col-span-1 !bg-white"
        />

        <Select
          label="نوع تراکنش"
          onChange={onTransactionTypeChange}
          value={transactionType}
          items={[
            {
              id: SATNA,
              name: SATNA_TITLE,
            },
            {
              id: PAYA,
              name: PAYA_TITLE,
            },
            {
              id: INTERNAL,
              name: INTERNAL_TITLE,
            },
            {
              id: INQUIRY_IBAN,
              name: INQUIRY_IBAN_TITLE,
            },
          ]}
          className="!col-span-1 !bg-white"
        />

        <Select
          label="وضعیت"
          onChange={onIsActiveChange}
          value={isActive}
          items={[
            {
              id: 1,
              name: "فعال",
            },
            {
              id: 2,
              name: "غیر فعال",
            },
          ]}
          className="!col-span-1 !bg-white"
        />
      </div>
    </FilterContainer>
  );
}
