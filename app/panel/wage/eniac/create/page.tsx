"use client";

import { useState, useEffect, Fragment } from "react";
import { useRouter } from "next/navigation";
import { useForm, SubmitHandler, Controller } from "react-hook-form";
import { toast } from "react-toastify";
import PanelBox from "@/components/panelBox";
import FormTitle from "@/components/formTitle";
import MaterialTextField from "@/components/materialTextField";
import Select from "@/components/select";
import Button from "@/components/button";
import { getCompanyListApi } from "@/services/company";
import { CompanyListType } from "@/services/company/types";
import { createCompanyWageApi } from "@/services/wage";
import {
  CONSTANT,
  CONSTANT_TITLE,
  PERCENT,
  PERCENT_TITLE,
  LEVEL,
  LEVEL_TITLE,
} from "@/enums/wageTypeEnums";
import {
  SATNA,
  SATNA_TITLE,
  PAYA,
  PAYA_TITLE,
  INTERNAL,
  INTERNAL_TITLE,
  INQUIRY_IBAN,
  INQUIRY_IBAN_TITLE,
} from "@/enums/transactionTypeEnums";

interface IFormInput {
  wage_type: 0 | 1 | 2 | string;
  amount?: string;
  transaction_type: 0 | 1 | 2 | 3 | string;
  is_active?: boolean;
  company: number | string;
  wage_level_type: 0 | 1 | string;
  wage_level: {
    amount: string;
    from_value: string;
    to_value: string;
  }[];
}

export default function CreateEniacWagePage() {
  const [loading, setLoading] = useState(false);
  const [companyList, setCompanyList] = useState<CompanyListType[]>([]);

  const router = useRouter();

  const {
    handleSubmit,
    control,
    watch,
    reset,
    setValue,
    getValues,
    formState: { errors },
  } = useForm<IFormInput>({
    defaultValues: {
      wage_type: "",
      transaction_type: "",
      company: "",
      amount: "",
      wage_level_type: "",
      wage_level: [],
    },
  });

  async function getCompanyList() {
    setLoading(true);

    const res = await getCompanyListApi();

    if (res) {
      setCompanyList(res?.data?.data);
    }

    setLoading(false);
  }

  useEffect(() => {
    getCompanyList();
  }, []);

  useEffect(() => {
    if (watch("wage_type") === LEVEL && watch("wage_level").length === 0) {
      setValue("wage_level", [
        {
          amount: "",
          from_value: "",
          to_value: "",
        },
      ]);
    } else {
      setValue("wage_level", []);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [watch("wage_type")]);

  const onSubmit: SubmitHandler<IFormInput> = async (data) => {
    setLoading(true);

    const res = await createCompanyWageApi({
      wage_type: typeof data.wage_type !== "string" ? data.wage_type : 0,
      ...(typeof data.wage_level_type !== "string" && {
        wage_level_type: data.wage_level_type,
      }),
      transaction_type:
        typeof data.transaction_type !== "string" ? data.transaction_type : 0,
      company: typeof data.company !== "string" ? data.company : 0,
      ...(data.amount && { amount: parseInt(data.amount) }),
      ...(data.wage_level.length > 0 && {
        wage_level: data.wage_level.map((item) => {
          return {
            from_value: parseInt(item.from_value),
            to_value: parseInt(item.to_value),
            amount: parseInt(item.amount),
          };
        }),
      }),
    });

    if (res) {
      reset();
      toast.success("کارمزد انیاکی با موفقیت ثبت شد");
    }

    setLoading(false);
  };

  function getAmountInputLabel() {
    switch (watch("wage_type")) {
      case CONSTANT:
        return "مبلغ کارمزد";

      case PERCENT:
        return "درصد کارمزد";

      default:
        return "مبلغ کارمزد";
    }
  }

  return (
    <PanelBox>
      <FormTitle title="تعریف کارمزد انیاک" />

      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="grid grid-cols-1 gap-4 xl:grid-cols-3">
          <Controller
            name="company"
            control={control}
            rules={{
              required: "شرکت را انتخاب نمایید.",
            }}
            render={({ field: { onChange, value } }) => (
              <Select
                label="شرکت *"
                className="!col-span-1"
                onChange={onChange}
                value={value}
                items={companyList.map((item) => {
                  return {
                    id: item.id,
                    name: item.name,
                  };
                })}
                error={!!errors?.company}
                helperText={errors?.company?.message}
              />
            )}
          />

          <Controller
            name="wage_type"
            control={control}
            rules={{
              required: "نوع کارمزد را انتخاب نمایید.",
            }}
            render={({ field: { onChange, value } }) => (
              <Select
                label="نوع کارمزد *"
                className="!col-span-1"
                onChange={onChange}
                value={value}
                items={[
                  {
                    id: CONSTANT,
                    name: CONSTANT_TITLE,
                  },
                  {
                    id: PERCENT,
                    name: PERCENT_TITLE,
                  },
                  {
                    id: LEVEL,
                    name: LEVEL_TITLE,
                  },
                ]}
                error={!!errors?.wage_type}
                helperText={errors?.wage_type?.message}
              />
            )}
          />

          <Controller
            name="transaction_type"
            control={control}
            rules={{
              required: "نوع تراکنش را انتخاب نمایید.",
            }}
            render={({ field: { onChange, value } }) => (
              <Select
                label="نوع تراکنش *"
                className="!col-span-1"
                onChange={onChange}
                value={value}
                items={[
                  {
                    id: SATNA,
                    name: SATNA_TITLE,
                  },
                  {
                    id: PAYA,
                    name: PAYA_TITLE,
                  },
                  {
                    id: INTERNAL,
                    name: INTERNAL_TITLE,
                  },
                  {
                    id: INQUIRY_IBAN,
                    name: INQUIRY_IBAN_TITLE,
                  },
                ]}
                error={!!errors?.transaction_type}
                helperText={errors?.transaction_type?.message}
              />
            )}
          />

          {watch("wage_type") === LEVEL ? (
            <Controller
              name="wage_level_type"
              control={control}
              rules={{
                required: "نحوه محاسبه را انتخاب نمایید.",
              }}
              render={({ field: { onChange, value } }) => (
                <Select
                  label="نحوه محاسبه *"
                  className="!col-span-1"
                  onChange={onChange}
                  value={value}
                  items={[
                    {
                      id: CONSTANT,
                      name: CONSTANT_TITLE,
                    },
                    {
                      id: PERCENT,
                      name: PERCENT_TITLE,
                    },
                  ]}
                  error={!!errors?.wage_level_type}
                  helperText={errors?.wage_level_type?.message}
                />
              )}
            />
          ) : (
            <Controller
              name="amount"
              control={control}
              // rules={{
              //   required: "عنوان حساب را وارد نمایید.",
              // }}
              render={({ field: { onChange, value } }) => (
                <MaterialTextField
                  label={getAmountInputLabel()}
                  value={value}
                  onChange={onChange}
                  className="!bg-white !col-span-1"
                  startAdornments={
                    watch("wage_type") === CONSTANT ? "ریال" : undefined
                  }
                  isFormatNumber={watch("wage_type") === CONSTANT}
                  error={!!errors?.amount}
                  helperText={errors?.amount?.message}
                />
              )}
            />
          )}

          {watch("wage_type") === LEVEL && (
            <Controller
              name="wage_level"
              control={control}
              rules={{
                required: "از مبلغ را وارد نمایید.",
              }}
              render={({ field: { onChange, value } }) => (
                <>
                  {watch("wage_level")?.map((item, key) => (
                    <div
                      className="bg-form p-4 rounded col-span-1 xl:col-span-3 flex flex-col xl:flex-row items-center gap-4"
                      key={key}
                    >
                      <MaterialTextField
                        label="از مبلغ"
                        value={value && value[key].from_value}
                        onChange={(v) => {
                          const _wageLevel = value;
                          _wageLevel[key].from_value = v.target.value;
                          onChange(_wageLevel);
                        }}
                        className="!bg-white !col-span-1"
                        startAdornments="ریال"
                        isFormatNumber
                        error={!!errors?.wage_level}
                        helperText={errors?.wage_level?.message}
                      />

                      <MaterialTextField
                        label="تا مبلغ"
                        value={value && value[key].to_value}
                        onChange={(v) => {
                          const _wageLevel = value;
                          _wageLevel[key].to_value = v.target.value;
                          onChange(_wageLevel);
                        }}
                        className="!bg-white !col-span-1"
                        startAdornments="ریال"
                        isFormatNumber
                        error={!!errors?.wage_level}
                        helperText={errors?.wage_level?.message}
                      />

                      <MaterialTextField
                        label={
                          watch("wage_level_type") === CONSTANT
                            ? "مبلغ کارمزد"
                            : "درصد کارمزد"
                        }
                        value={value && value[key].amount}
                        onChange={(v) => {
                          const _wageLevel = value;
                          _wageLevel[key].amount = v.target.value;
                          onChange(_wageLevel);
                        }}
                        className="!bg-white !col-span-1"
                        startAdornments={
                          watch("wage_level_type") === CONSTANT
                            ? "ریال"
                            : undefined
                        }
                        isFormatNumber={watch("wage_level_type") === CONSTANT}
                        error={!!errors?.wage_level}
                        helperText={errors?.wage_level?.message}
                      />

                      {key === 0 ? (
                        <Button
                          fullRounded={false}
                          className="!min-w-28"
                          onClick={() => {
                            const _wageLevel = getValues("wage_level");
                            _wageLevel?.push({
                              amount: "",
                              from_value: "",
                              to_value: "",
                            });
                            setValue("wage_level", _wageLevel);
                          }}
                        >
                          ایجاد پله
                        </Button>
                      ) : (
                        <Button
                          fullRounded={false}
                          variant="cancel"
                          className="!min-w-28"
                          onClick={() => {
                            const _wageLevel = getValues("wage_level");
                            _wageLevel && _wageLevel.splice(key, 1);
                            setValue("wage_level", _wageLevel);
                          }}
                        >
                          حذف پله
                        </Button>
                      )}
                    </div>
                  ))}
                </>
              )}
            />
          )}
        </div>

        <div className="grid grid-cols-2 lg:grid-cols-8 gap-4 mt-8 w-full">
          <Button
            fullRounded={false}
            className="!col-span-1 lg:!col-start-7"
            type="submit"
            loading={loading}
          >
            ثبت
          </Button>
          <Button
            variant="cancel"
            className="!col-span-1 lg:!col-start-8"
            loading={loading}
            onClick={() => router.push("/panel/wage/eniac/list")}
          >
            انصراف
          </Button>
        </div>
      </form>
    </PanelBox>
  );
}
