import WageMenu from "./components/menu";

export default function ChargeLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <div>
      <WageMenu />

      <div className="mt-4 flex flex-col items-center">{children}</div>
    </div>
  );
}
