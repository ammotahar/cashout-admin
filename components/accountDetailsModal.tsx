"use client";

import { useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import Modal from "@mui/material/Modal";
import Skeleton from "@mui/material/Skeleton";
import FormTitle from "./formTitle";
import Button from "./button";
import { getAccountDetailsApi } from "@/services/account";
import { AccountDetailsType } from "@/services/account/types";
import { jalaliConverter } from "@/utils/jalaliConverter";

const RowItem = ({
  title,
  value,
}: {
  title?: string;
  value?: string | React.ReactNode;
}) => {
  return (
    <>
      <div className="col-span-1 text-right">
        <span className="text-black text-sm">{title}</span>
      </div>

      <div className="col-span-1 text-right lg:text-left">
        <span className="text-gray text-sm">{value}</span>
      </div>
    </>
  );
};

export default function AccountDetailsModal({
  open,
  onClose,
  accountId,
}: {
  open: boolean;
  onClose?: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  accountId: number;
}) {
  const [data, setData] = useState<AccountDetailsType | undefined>();
  const [loading, setLoading] = useState(false);

  const router = useRouter();

  async function getAccountDetails(id: number) {
    setLoading(true);

    const res = await getAccountDetailsApi(id);

    if (res) {
      setData(res?.data);
    }

    setLoading(false);
  }

  useEffect(() => {
    if (open && accountId) {
      getAccountDetails(accountId);
    }
  }, [accountId, open]);

  return (
    <Modal
      open={open}
      onClose={onClose}
      className="!flex !items-center !justify-center"
    >
      {loading ? (
        <div className="bg-white flex flex-col items-center min-w-72 min-h-56 rounded-xl border-2 border-primary p-4 mx-4">
          <Skeleton variant="rounded" className="!w-full !h-96 !mt-4" />
        </div>
      ) : (
        <div className="bg-white flex flex-col items-center min-w-72 min-h-56 rounded-xl border-2 border-primary p-4 mx-4">
          <FormTitle
            title="مشاهده  جزئیات حساب"
            className="w-full !text-center"
          />

          <div className="p-4 mt-4 bg-gray rounded-xl shadow w-full">
            <div className="grid grid-cols-1 lg:grid-cols-2 gap-4 max-h-96 overflow-auto p-4">
              <RowItem title="عنوان حساب:" value={data?.account_name} />
              <RowItem title="شماره حساب:" value={data?.account_number} />
              <RowItem title="نام صاحب حساب:" value={data?.full_name} />
              <RowItem title="شماره شبا:" value={data?.iban} />
              <RowItem title="شماره مشتری:" value={data?.customer_code} />
              <RowItem title="نام بانک:" value={data?.bank_name} />
              <RowItem title="نام کاربری:" value={data?.user_name} />
              <RowItem title="رمز عبور:" value={data?.password} />
              <RowItem
                title="نام کانال (Channel Name):"
                value={data?.channel_name}
              />
              <RowItem
                title="کلید رمز (Secret Key):"
                value={data?.secret_key}
              />
              <RowItem
                title="تاریخ ثبت:"
                value={data?.create_time && jalaliConverter(data?.create_time)}
              />
              <RowItem title="وضعیت حساب:" value={data?.account_status} />
              <RowItem
                title="امکان مشاهده در داشبورد:"
                value={data?.view_dashboard === true ? "فعال" : "غیر فعال"}
              />
              <RowItem title="توضیحات:" value={data?.description} />
            </div>
          </div>

          <div className="grid grid-cols-2 lg:grid-cols-4 gap-4 mt-4 w-full">
            <Button
              fullRounded={false}
              className="!col-span-1 lg:!col-start-3"
              onClick={() =>
                router.push(`/panel/account/create?id=${accountId}`)
              }
            >
              ویرایش اطلاعات
            </Button>
            <Button
              variant="cancel"
              className="!col-span-1 lg:!col-start-4"
              onClick={(event) => onClose && onClose(event, "backdropClick")}
            >
              بستن
            </Button>
          </div>
        </div>
      )}
    </Modal>
  );
}
