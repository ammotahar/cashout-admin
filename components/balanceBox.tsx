"use client";

import { useState, useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import Skeleton from "@mui/material/Skeleton";
import Tabs from "@/components/tabs";
import PanelBox from "@/components/panelBox";
import PriceText from "@/components/priceText";
import { getBalanceAndMinDepositApi } from "@/services/company";
import ActiveCardImage from "assets/images/active-card.svg";
import PlusIcon from "@/assets/icons/blue/plus";

export default function BalanceBox() {
  const [value, setValue] = useState(0);
  const [balance, setBalance] = useState(0);
  const [balanceLoading, setBalanceLoading] = useState(false);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  async function getBalanceAndMinDeposit() {
    setBalanceLoading(true);
    const res = await getBalanceAndMinDepositApi();

    if (res) {
      setBalance(res?.data?.balance);
    }

    setBalanceLoading(false);
  }

  useEffect(() => {
    getBalanceAndMinDeposit();
  }, []);

  return (
    <PanelBox>
      <div className="w-full h-full">
        <Tabs value={value} onChange={handleChange} tabs={["کیف پول"]} />

        <div className="w-full md:w-1/2 lg:w-4/5 xl:w-3/5 mx-auto mt-4">
          {balanceLoading ? (
            <Skeleton
              variant="rounded"
              className="!w-full !h-32 !rounded-2xl !mt-4"
            />
          ) : (
            <>
              {/* <Link
                className="flex items-center justify-center bg-gray rounded-lg py-3"
                href="/panel/charge/receipt"
              >
                <PlusIcon />
                <span className="text-xs mr-2">افزودن موجودی</span>
              </Link> */}

              <div className="w-full mt-4 relative flex items-center justify-center">
                <PriceText
                  price={balance.toString()}
                  containerClassName="absolute"
                />
                <Image
                  src={ActiveCardImage}
                  alt="balance-card"
                  className="w-full"
                />
              </div>
            </>
          )}
        </div>
      </div>
    </PanelBox>
  );
}
