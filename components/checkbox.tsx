import { ChangeEvent } from "react";
import MuiCheckbox from "@mui/material/Checkbox";

export default function Checkbox({
  title,
  className,
  titleClassName,
  value,
  onChange,
}: {
  title?: string;
  className?: string;
  titleClassName?: string;
  value?: any;
  onChange?: (event: ChangeEvent<HTMLInputElement>, checked: boolean) => void;
}) {
  return (
    <div className={`flex items-center ${className}`}>
      <MuiCheckbox
        className="!m-0 !p-0 !text-primary"
        checked={value}
        onChange={onChange}
      />
      {title && <span className={`${titleClassName} mr-1`}>{title}</span>}
    </div>
  );
}
