"use client";

import { useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import Modal from "@mui/material/Modal";
import Skeleton from "@mui/material/Skeleton";
import FormTitle from "./formTitle";
import Button from "./button";
import { getCompanyDetailsApi } from "@/services/company";
import { CompanyDetailsType } from "@/services/company/types";
import { rialSeparator } from "@/utils/rialSeperator";
import { jalaliConverter } from "@/utils/jalaliConverter";

const RowItem = ({
  title,
  value,
}: {
  title?: string;
  value?: string | React.ReactNode;
}) => {
  return (
    <>
      <div className="col-span-1 text-right">
        <span className="text-black text-sm">{title}</span>
      </div>

      <div className="col-span-1 text-right lg:text-left">
        <span className="text-gray text-sm">{value}</span>
      </div>
    </>
  );
};

export default function CompanyDetailsModal({
  open,
  onClose,
  companyId,
}: {
  open: boolean;
  onClose?: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  companyId: number;
}) {
  const [data, setData] = useState<CompanyDetailsType | undefined>();
  const [loading, setLoading] = useState(false);

  const router = useRouter();

  async function getCompanyDetails(id: number) {
    setLoading(true);

    const res = await getCompanyDetailsApi(id);

    if (res) {
      setData(res?.data);
    }

    setLoading(false);
  }

  useEffect(() => {
    if (open && companyId) {
      getCompanyDetails(companyId);
    }
  }, [companyId, open]);

  return (
    <Modal
      open={open}
      onClose={onClose}
      className="!flex !items-center !justify-center"
    >
      {loading ? (
        <div className="bg-white flex flex-col items-center min-w-72 min-h-56 rounded-xl border-2 border-primary p-4 mx-4">
          <Skeleton variant="rounded" className="!w-full !h-96 !mt-4" />
        </div>
      ) : (
        <div className="bg-white flex flex-col items-center min-w-72 min-h-56 rounded-xl border-2 border-primary p-4 mx-4">
          <FormTitle
            title="مشاهده جزئیات شرکت"
            className="w-full !text-center"
          />

          <div className="p-4 mt-4 bg-gray rounded-xl shadow w-full">
            <div className="grid grid-cols-1 lg:grid-cols-2 gap-4 max-h-96 overflow-auto p-4">
              <RowItem title="نام شرکت:" value={data?.name} />

              <RowItem title="کد شرکت:" value={data?.company_code} />

              <RowItem title="تلفن شرکت:" value={data?.phone} />

              <RowItem
                title="موجودی شرکت:"
                value={rialSeparator(data?.balance.toString())}
              />

              <RowItem
                title="تاریخ آخرین بروزرسانی:"
                value={data?.update_date && jalaliConverter(data?.update_date)}
              />

              <RowItem
                title="مبلغ بلوکه شده سرکت:"
                value={
                  data?.min_deposit &&
                  rialSeparator(data.min_deposit.toString())
                }
              />

              <RowItem
                title="مجوز تعریف تعداد کاربران شرکت:"
                value={data?.number_create_user}
              />

              <RowItem title="توضیحات:" value={data?.description} />

              <RowItem
                title="وضعیت:"
                value={data?.status === true ? "فعال" : "غیر فعال"}
              />
            </div>
          </div>

          <div className="grid grid-cols-2 lg:grid-cols-4 gap-4 mt-4 w-full">
            <Button
              fullRounded={false}
              className="!col-span-1 lg:!col-start-3"
              onClick={() =>
                router.push(`/panel/company/create?id=${companyId}`)
              }
            >
              ویرایش اطلاعات
            </Button>
            <Button
              variant="cancel"
              className="!col-span-1 lg:!col-start-4"
              onClick={(event) => onClose && onClose(event, "backdropClick")}
            >
              بستن
            </Button>
          </div>
        </div>
      )}
    </Modal>
  );
}
