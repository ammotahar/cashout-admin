"use client";

import { useState } from "react";
import Image from "next/image";
import Modal from "@mui/material/Modal";
import FormTitle from "./formTitle";
import Button from "./button";
import { deactiveCompanyWageApi, deactiveBankWageApi } from "@/services/wage";
import WarningIcon from "assets/icons/warning.svg";

export default function DeactiveWageModal({
  open,
  onClose,
  wageId,
  onFinish,
  type,
}: {
  open: boolean;
  onClose?: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  wageId: number;
  onFinish?: () => void;
  type: "company" | "bank";
}) {
  const [loading, setLoading] = useState(false);

  async function deactiveCompanyWage() {
    setLoading(true);

    const res =
      type === "company"
        ? await deactiveCompanyWageApi({ is_active: false }, wageId)
        : await deactiveBankWageApi({ is_active: false }, wageId);

    if (res) {
      onFinish && onFinish();
    }

    setLoading(false);
  }

  return (
    <Modal
      open={open}
      onClose={onClose}
      className="!flex !items-center !justify-center"
    >
      <div className="bg-white flex flex-col items-center min-w-72 min-h-56 rounded-xl border-2 border-primary p-4 mx-4">
        <FormTitle
          title="غیرفعال کردن کارمزد"
          className="w-full !text-center"
        />

        <div className="p-4 bg-gray rounded-xl shadow w-full flex flex-col items-center gap-4 text-black">
          <Image src={WarningIcon} alt="warning" />

          <div className="text-center">
            <span>آیا از غیر فعال کردن کارمزد اطمینان دارید؟</span>
          </div>
        </div>

        <div className="grid grid-cols-2 lg:grid-cols-4 gap-4 mt-4 w-full">
          <Button
            fullRounded={false}
            className="!col-span-1 lg:!col-start-3"
            onClick={deactiveCompanyWage}
            loading={loading}
          >
            بله
          </Button>
          <Button
            variant="cancel"
            className="!col-span-1 lg:!col-start-4"
            onClick={(event) => onClose && onClose(event, "backdropClick")}
          >
            انصراف
          </Button>
        </div>
      </div>
    </Modal>
  );
}
