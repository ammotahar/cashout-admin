"use client";

import { useCallback, useEffect, useState } from "react";
import Image from "next/image";
import { useForm, Controller, SubmitHandler } from "react-hook-form";
import Modal from "@mui/material/Modal";
import { CircularProgress } from "@mui/material";
import FormTitle from "./formTitle";
import TextField from "@/components/textField";
import Button from "./button";
import Otp from "@/components/otp";
import { sendOtpLoginApi, verifyOtpApi } from "@/services/authorization";
import { deleteAccountApi, deactiveAccountApi } from "@/services/account";
import { PersonIcon } from "@/assets/icons/black/person";
import { PasswordIcon } from "@/assets/icons/black/password";
import WarningIcon from "assets/icons/warning.svg";

interface IFormInput {
  username: string;
  password: string;
}

export default function DeleteOrDeactiveAccountModal({
  type,
  open,
  onClose,
  accountId,
  onFinish,
}: {
  type: "delete" | "deactive";
  open: boolean;
  onClose?: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  accountId: number;
  onFinish: () => void;
}) {
  const [loading, setLoading] = useState(false);
  const [step, setStep] = useState<"attempt" | "login" | "otp">("attempt");
  const [showTimer, setShowTimer] = useState(true);
  const [otpCode, setOtpCode] = useState("");

  const {
    handleSubmit,
    control,
    getValues,
    formState: { errors },
  } = useForm({
    defaultValues: {
      username: "",
      password: "",
    },
  });

  const onSubmit: SubmitHandler<IFormInput> = async (data) => {
    setLoading(true);

    const res = await sendOtpLoginApi({
      ...data,
      is_admin: true,
      service_type: type === "delete" ? 2 : 3,
    });

    setLoading(false);

    if (res) {
      setStep("otp");
    }

    setLoading(false);
  };

  const verifyOtp = useCallback(async () => {
    setLoading(true);

    const res = await verifyOtpApi({
      username: getValues("username"),
      otp: otpCode,
      is_admin: true,
      service_type: type === "delete" ? 2 : 3,
    });

    if (res) {
      if (type === "delete") {
        const deleteAccount = await deleteAccountApi(
          { id: accountId },
          res?.data?.ott_token
        );
      } else if (type === "deactive") {
        const deactiveAccount = await deactiveAccountApi(
          { account_id: accountId },
          res?.data?.ott_token
        );
      }

      setOtpCode("");
      setStep("attempt");
      onFinish();
      onClose && onClose({}, "backdropClick");
    }

    setLoading(false);
  }, [accountId, getValues, onClose, onFinish, otpCode, type]);

  useEffect(() => {
    if (otpCode.length === 6) {
      verifyOtp();
    }
  }, [otpCode, verifyOtp]);

  function renderContent() {
    switch (step) {
      case "attempt":
        return (
          <div className="p-4 bg-gray rounded-xl shadow w-full flex flex-col items-center gap-4 text-black">
            <Image src={WarningIcon} alt="warning" />

            <div className="text-center">
              <span>
                {type === "delete"
                  ? "آیا از حذف حساب اطمینان دارید؟"
                  : "آیا از غیر فعال کردن حساب اطمینان دارید؟"}
              </span>
            </div>
          </div>
        );

      case "login":
        return (
          <form
            className="flex flex-col bg-gray rounded-xl shadow p-4"
            onSubmit={handleSubmit(onSubmit)}
          >
            <div className="mt-4">
              <div>
                <span className="text-black text-xs">
                  لطفا نام کاربری و کلمه عبوری که در هنگام ثبت نام تعیین کرده
                  اید را وارد نمایید.
                </span>
              </div>

              <Controller
                name="username"
                control={control}
                rules={{
                  required: "نام کاربری را وارد نمایید.",
                }}
                render={({ field: { onChange, value } }) => (
                  <TextField
                    title="نام کاربری"
                    value={value}
                    onChange={onChange}
                    icon={<PersonIcon />}
                    className="mb-4 mt-8"
                    inputClassName="bg-transparent"
                    error={!!errors?.username}
                    helperText={errors?.username?.message}
                  />
                )}
              />

              <Controller
                name="password"
                control={control}
                rules={{
                  required: "رمز عبور را وارد نمایید.",
                }}
                render={({ field: { onChange, value } }) => (
                  <TextField
                    title="کلمه عبور"
                    value={value}
                    onChange={onChange}
                    icon={<PasswordIcon />}
                    className="mb-4 mt-8"
                    inputClassName="bg-transparent"
                    type="password"
                    error={!!errors?.password}
                    helperText={errors?.password?.message}
                  />
                )}
              />
            </div>

            <div className="grid grid-cols-2 lg:grid-cols-4 gap-4 mt-4 w-full">
              <Button
                fullRounded={false}
                className="!col-span-1 lg:!col-start-3"
                type="submit"
                loading={loading}
              >
                مرحله بعد
              </Button>
              <Button
                variant="cancel"
                className="!col-span-1 lg:!col-start-4"
                onClick={(event) => {
                  setStep("attempt");
                  onClose && onClose(event, "backdropClick");
                }}
                loading={loading}
              >
                انصراف
              </Button>
            </div>
          </form>
        );

      case "otp":
        return (
          <div className="flex flex-col items-center">
            <div>
              <span className="text-subtitle text-xs">
                کد یکبار مصرف به شماره موبایل ارسال شده است.
              </span>
            </div>

            <Otp
              //TODO: Add resend functionality
              onResendOtp={() => {}}
              showTimer={showTimer}
              setShowTimer={setShowTimer}
              otpCode={otpCode}
              setOtpCode={setOtpCode}
            />

            {loading && <CircularProgress className="!mt-4" />}
          </div>
        );

      default:
        break;
    }
  }

  return (
    <Modal
      open={open}
      onClose={onClose}
      className="!flex !items-center !justify-center"
    >
      <div className="bg-white flex flex-col items-center min-w-72 min-h-56 rounded-xl border-2 border-primary p-4 mx-4">
        <FormTitle
          title={type === "delete" ? "حذف حساب" : "غیرفعال کردن حساب"}
          className="w-full !text-center"
        />

        {renderContent()}

        {step === "attempt" && (
          <div className="grid grid-cols-2 lg:grid-cols-4 gap-4 mt-4 w-full">
            <Button
              fullRounded={false}
              className="!col-span-1 lg:!col-start-3"
              onClick={() => setStep("login")}
            >
              بله
            </Button>
            <Button
              variant="cancel"
              className="!col-span-1 lg:!col-start-4"
              onClick={(event) => onClose && onClose(event, "backdropClick")}
            >
              خیر
            </Button>
          </div>
        )}
      </div>
    </Modal>
  );
}
