"use client";

import { useEffect, useState } from "react";
import Image from "next/image";
import Modal from "@mui/material/Modal";
import FormTitle from "./formTitle";
import Button from "./button";
import { getIbanInfoApi } from "@/services/account";
import { IbanInfoType } from "@/services/account/types";
import WarningIcon from "assets/icons/warning.svg";

export default function IbanInfoModal({
  open,
  onClose,
  iban,
  onCompleteInquiry,
  onConfirmData,
}: {
  open: boolean;
  onClose?: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  iban: string;
  onCompleteInquiry: (data: IbanInfoType) => void;
  onConfirmData: (data: IbanInfoType) => void;
}) {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<IbanInfoType | undefined>();

  async function getIbanInfo() {
    setLoading(true);

    const res = await getIbanInfoApi({ iban });

    if (res) {
      setData(res?.data);
      onCompleteInquiry(res?.data);
    }

    setLoading(false);
  }

  useEffect(() => {
    setData(undefined);
  }, [iban]);

  return (
    <Modal
      open={open}
      onClose={onClose}
      className="!flex !items-center !justify-center"
    >
      <div className="bg-white flex flex-col items-center min-w-72 min-h-56 rounded-xl border-2 border-primary p-4 mx-4">
        <FormTitle title="استعلام شماره شبا" className="w-full !text-center" />

        <div className="p-4 bg-gray rounded-xl shadow w-full flex flex-col items-center gap-4 text-black">
          {data ? (
            <>
              <div>
                <span>شماره شبا:</span>
              </div>

              <div>
                <span>{iban}</span>
              </div>

              <div>
                <span>به نام: {`${data.first_name} ${data.last_name}`}</span>
              </div>

              <div>
                <span>شماره حساب: {data.account_number}</span>
              </div>

              <div>
                <span>نام بانک: {data.bank}</span>
              </div>
            </>
          ) : (
            <>
              <Image src={WarningIcon} alt="warning" />

              <div className="text-center">
                <span>هزینه استعلام شماره شبا رایگان می‌باشد.</span>
              </div>
            </>
          )}
        </div>

        <div className="grid grid-cols-2 lg:grid-cols-4 gap-4 mt-4 w-full">
          <Button
            fullRounded={false}
            className="!col-span-1 lg:!col-start-3"
            onClick={(event) => {
              if (data) {
                onConfirmData(data);
                onClose && onClose(event, "backdropClick");
              } else {
                getIbanInfo();
              }
            }}
            loading={loading}
          >
            {data ? "تایید و جایگزینی اطلاعات" : "استعلام"}
          </Button>
          <Button
            variant="cancel"
            className="!col-span-1 lg:!col-start-4"
            onClick={(event) => onClose && onClose(event, "backdropClick")}
            loading={loading}
          >
            انصراف
          </Button>
        </div>
      </div>
    </Modal>
  );
}
