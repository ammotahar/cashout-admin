"use client";

import React, { Dispatch, MouseEventHandler, SetStateAction } from "react";
import OtpInput from "react-otp-input";
import { CircularProgress } from "@mui/material";
import Timer from "./timer";
import SyncIcon from "assets/icons/blue/sync";

const Otp = ({
  showTimer,
  setShowTimer,
  onResendOtp,
  otpCode,
  setOtpCode,
  resendLoading = false,
}: {
  onResendOtp: MouseEventHandler<HTMLDivElement>;
  showTimer: boolean;
  setShowTimer: Dispatch<SetStateAction<boolean>>;
  otpCode: string;
  setOtpCode: Dispatch<SetStateAction<string>>;
  resendLoading?: boolean;
}) => {
  return (
    <div className="mt-3">
      <div
        className="flex flex-col items-center justify-center"
        style={{ direction: "ltr" }}
      >
        <OtpInput
          value={otpCode}
          onChange={(value) => setOtpCode(value)}
          numInputs={6}
          shouldAutoFocus={true}
          // isInputNum={true}
          // containerStyle="otpContainerStyle"
          inputStyle="border border-primary rounded m-2 !w-10 !h-12 focus-visible:border-secondary focus-visible:border-2 focus-visible:outline-none"
          renderInput={(props) => <input {...props} inputMode="numeric" />}
          // hasErrored={error}
        />
      </div>

      {showTimer ? (
        <Timer setActive={setShowTimer} initialMinute={1} initialSeconds={30} />
      ) : resendLoading ? (
        <div className="flex items-center justify-center mt-8">
          <CircularProgress />
        </div>
      ) : (
        <div
          className="flex items-center justify-center mt-8 cursor-pointer"
          onClick={onResendOtp}
        >
          <SyncIcon />
          <div className="text-black mr-2">ارسال مجدد کد</div>
        </div>
      )}
    </div>
  );
};

export default Otp;
