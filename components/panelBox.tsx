export default function PanelBox({
  children,
  className,
}: {
  children: React.ReactNode;
  className?: string;
}) {
  return (
    <div
      className={`w-full h-full p-4 bg-white rounded shadow-panelContent overflow-auto ${className}`}
    >
      {children}
    </div>
  );
}
