import { ChangeEvent } from "react";
import MuiRadio from "@mui/material/Radio";

export default function Radio({
  label,
  className,
  checked,
  handleChange,
}: {
  label: string;
  className?: string;
  checked?: boolean;
  handleChange?: (
    event: ChangeEvent<HTMLInputElement>,
    checked: boolean
  ) => void;
}) {
  return (
    <div className={`flex items-center ${className}`}>
      <MuiRadio checked={checked} onChange={handleChange} />
      <span>{label}</span>
    </div>
  );
}
