"use client";

import { useEffect, useState } from "react";
import Image from "next/image";
import Modal from "@mui/material/Modal";
import Skeleton from "@mui/material/Skeleton";
import Collapse from "@mui/material/Collapse";
import { toast } from "react-toastify";
import Button from "./button";
import Select from "./select";
import FormTitle from "./formTitle";
import MaterialTextField from "./materialTextField";
import {
  getChargeBalanceDetailsApi,
  confirmChargeBalanceApi,
} from "@/services/charge";
import { ChargeBalanceDetailsType } from "@/services/charge/types";
import { getChargeBalanceStatusTitle } from "@/utils/getChargeBalanceStatusTitle";
import { rialSeparator } from "@/utils/rialSeperator";
import { jalaliConverter } from "@/utils/jalaliConverter";
import { NOT_CONFIRMED, CONFIRMED } from "@/enums/chargeBalanceStatusEnums";
import DownloadImage from "assets/images/download.svg";

const RowItem = ({
  title,
  value,
}: {
  title?: string;
  value?: string | React.ReactNode;
}) => {
  return (
    <>
      <div className="col-span-1 text-right">
        <span className="text-black text-sm">{title}</span>
      </div>

      <div className="col-span-1 text-right lg:text-left">
        <span className="text-gray text-sm">{value}</span>
      </div>
    </>
  );
};

export default function ReceiptDetailsModal({
  open,
  onClose,
  chargeId,
  onConfirmComplete,
}: {
  open: boolean;
  onClose?: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  chargeId?: number;
  onConfirmComplete?: () => void;
}) {
  const [data, setData] = useState<ChargeBalanceDetailsType | undefined>();
  const [loading, setLoading] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [rejectResult, setRejectResult] = useState("");
  const [requestStatus, setRequestStatus] = useState<string | number>("");

  async function getChargeBalanceDetails(id: number) {
    setLoading(true);

    const res = await getChargeBalanceDetailsApi(id);

    if (res) {
      setData(res?.data);
    }

    setLoading(false);
  }

  useEffect(() => {
    if (chargeId && open) {
      getChargeBalanceDetails(chargeId);
    }
  }, [chargeId, open]);

  async function confirmChargeBalance(
    status: number,
    id: number,
    rejectResult?: string
  ) {
    setConfirmLoading(true);

    const res = await confirmChargeBalanceApi({
      status,
      charge_balance_id: id,
      reject_result: rejectResult,
    });

    if (res) {
      setRejectResult("");
      setRequestStatus("");
      onConfirmComplete && onConfirmComplete();
    }

    setConfirmLoading(false);
  }

  function changeRequestStatus() {
    if (requestStatus === NOT_CONFIRMED) {
      if (rejectResult.length > 0) {
        chargeId && confirmChargeBalance(NOT_CONFIRMED, chargeId, rejectResult);
      } else {
        toast.error("علت رد درخواست را وارد نمایید.");
      }
    } else if (requestStatus === CONFIRMED) {
      chargeId && confirmChargeBalance(CONFIRMED, chargeId);
    } else {
      toast.error("ابتدا وضعیت درخواست را انتخاب کنید.");
    }
  }

  return (
    <Modal
      open={open}
      onClose={onClose}
      className="!flex !items-center !justify-center"
    >
      {loading ? (
        <div className="bg-white flex flex-col items-center min-w-72 min-h-56 rounded-xl border-2 border-primary p-4 mx-4">
          <Skeleton variant="rounded" className="!w-full !h-96 !mt-4" />
        </div>
      ) : (
        <div className="bg-white flex flex-col items-center min-w-72 min-h-56 rounded-xl border-2 border-primary p-4 mx-4">
          <FormTitle
            title="مشاهده  جزئیات درخواست واریز بانکی"
            className="w-full !text-center"
          />

          <div className="p-4 mt-4 bg-gray rounded-xl shadow">
            <div className="grid grid-cols-1 lg:grid-cols-2 gap-4 max-h-96 overflow-auto p-4">
              <RowItem
                title="نام و نام خانوادگی / نام شرکت کاربر بارگذاری کننده فیش بانکی:"
                value={data?.user_name}
              />

              <RowItem
                title="نام و نام خانوادگی / نام شرکت کاربر تایید کننده فیش بانکی:"
                value={data?.confirmer_name || "-"}
              />

              <RowItem
                title="وضعیت:"
                value={
                  data?.status !== undefined
                    ? getChargeBalanceStatusTitle(data?.status)
                    : "-"
                }
              />

              <RowItem title="نام بانک:" value={data?.bank_title} />

              <RowItem title="طریقه افزایش موجودی:" value={data?.type_title} />

              <RowItem
                title="مبلغ واریزی:"
                value={rialSeparator(data?.amount.toString())}
              />

              <RowItem
                title="تاریخ ثبت:"
                value={data?.insert_date && jalaliConverter(data?.insert_date)}
              />

              <RowItem
                title="کد پیگیری فیش بارگذاری شده:"
                value={data?.track_number.toString()}
              />

              <RowItem
                title="تاریخ تایید:"
                value={
                  data?.confirm_date && jalaliConverter(data?.confirm_date)
                }
              />

              <RowItem title="علت عدم تایید:" value={data?.reject_result} />

              <RowItem
                title="کد پیگیری دریافت شده از درگاه:"
                value={data?.track_number.toString()}
              />
            </div>

            {data?.file_path && (
              <div className="flex items-center justify-center mt-6">
                <Button
                  variant="secondary"
                  onClick={() =>
                    data?.file_path && window.open(data?.file_path, "_blank")
                  }
                >
                  <div>
                    <span>دانلود عکس فیش واریزی</span>
                  </div>

                  <div className="mr-2">
                    <Image
                      src={DownloadImage}
                      alt="download"
                      className="w-8 h-8"
                    />
                  </div>
                </Button>
              </div>
            )}
          </div>

          {data?.status === 2 && (
            <>
              <div className="mt-4 w-full">
                <Select
                  label="تعیین وضعیت درخواست"
                  onChange={(event) => setRequestStatus(event.target.value)}
                  value={requestStatus}
                  items={[
                    { id: "", name: "تعیین وضعیت درخواست" },
                    { id: CONFIRMED, name: "تایید درخواست" },
                    { id: NOT_CONFIRMED, name: "رد درخواست" },
                  ]}
                />
              </div>
              <Collapse
                in={requestStatus === NOT_CONFIRMED}
                className="!mt-6 !w-full"
              >
                <MaterialTextField
                  label="علت رد درخواست *"
                  value={rejectResult}
                  onChange={(event) => setRejectResult(event.target.value)}
                  className="!bg-white"
                />
              </Collapse>
              <div className="grid grid-cols-2 lg:grid-cols-4 gap-4 mt-4 w-full">
                <Button
                  fullRounded={false}
                  className="!col-span-1 lg:!col-start-3"
                  onClick={changeRequestStatus}
                  loading={confirmLoading}
                >
                  تایید
                </Button>
                <Button
                  variant="cancel"
                  className="!col-span-1 lg:!col-start-4"
                  onClick={() => {
                    onClose && onClose({}, "backdropClick");
                  }}
                  loading={confirmLoading}
                >
                  انصراف
                </Button>
              </div>
            </>
          )}
        </div>
      )}
    </Modal>
  );
}
