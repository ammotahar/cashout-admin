"use client";

import { useState, useEffect } from "react";
import { InputAdornment, TextField } from "@mui/material";
import { Search } from "@mui/icons-material";

export default function SearchField({
  placeholder,
  onChange,
}: {
  placeholder?: string;
  onChange?: (value: string) => void;
}) {
  const [search, setSearch] = useState("");

  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      onChange && onChange(search);
    }, 500);

    return () => clearTimeout(delayDebounceFn);
  }, [search, onChange]);

  return (
    <TextField
      id="outlined-basic"
      label="جستجو"
      value={search}
      onChange={(event) => setSearch(event.target.value)}
      placeholder={placeholder}
      variant="outlined"
      size="small"
      className="!w-full"
      InputProps={{
        endAdornment: (
          <InputAdornment position="start">
            <Search />
          </InputAdornment>
        ),
        className: "!pl-0",
      }}
    />
  );
}
