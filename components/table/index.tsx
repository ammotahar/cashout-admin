import MuiTable from "@mui/material/Table";
import {
  TableContainer,
  TableBody,
  TableRow,
  TableCell,
  Skeleton,
} from "@mui/material";
import Head from "./head";
import Row from "./row";

export default function Table({
  heads,
  data,
  loading = false,
}: {
  heads: string[];
  data?: any[];
  loading?: boolean;
}) {
  if (loading) {
    return <Skeleton variant="rectangular" className="!w-full !h-[460px]" />;
  }
  return (
    <TableContainer className="border border-table rounded max-h-[460px]">
      <MuiTable sx={{ minWidth: 650 }} aria-label="sticky table" stickyHeader>
        <Head titles={heads} />
        <TableBody>
          {data && data?.length > 0 ? (
            data?.map((item, key) => <Row key={key} cells={item} index={key} />)
          ) : (
            <TableRow
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell align="center">داده ای یافت نشد</TableCell>
            </TableRow>
          )}
        </TableBody>
      </MuiTable>
    </TableContainer>
  );
}
