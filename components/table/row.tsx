import { TableRow, TableCell } from "@mui/material";
import InfoIcon from "@mui/icons-material/Info";

export default function Row({ cells, index }: { cells: any; index: number }) {
  return (
    <TableRow
      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
      className="!relative"
    >
      {Object.keys(cells).map(
        (item: any, index) =>
          item !== "hasError" && (
            <TableCell align="center" key={index} className="!text-xs">
              {cells[item]}
            </TableCell>
          )
      )}

      {cells?.hasError && (
        <>
          <div
            className={`border border-error ${
              index >= 1 && "border-t-0"
            } w-full h-full absolute right-0`}
          />

          <div className="absolute bottom-[-10px] right-0 flex items-center gap-2 text-error text-xs bg-white px-2 z-1 mr-4">
            <div>
              <InfoIcon className="!text-base" />
            </div>

            <div>
              <span>شماره شبا صحیح نیست!</span>
            </div>
          </div>
        </>
      )}
    </TableRow>
  );
}
