"use client";

import React, { useEffect, useState } from "react";

const Timer = ({
  initialMinute = 0,
  initialSeconds = 0,
  setActive,
}: {
  initialMinute: number;
  initialSeconds: number;
  setActive: React.Dispatch<React.SetStateAction<boolean>>;
}) => {
  const [minutes, setMinutes] = useState(initialMinute);
  const [seconds, setSeconds] = useState(initialSeconds);

  useEffect(() => {
    let myInterval = setInterval(() => {
      if (seconds > 0) {
        setSeconds(seconds - 1);
      }
      if (seconds === 0) {
        if (minutes === 0) {
          clearInterval(myInterval);
          setActive(false);
        } else {
          setMinutes(minutes - 1);
          setSeconds(59);
        }
      }
    }, 1000);
    return () => {
      clearInterval(myInterval);
    };
  });
  return (
    <div className="flex items-center justify-center text-center mt-8">
      <div>مدت زمان باقیمانده</div>
      <div className="mr-5">
        {minutes === 0 && seconds === 0 ? null : (
          <div>
            {minutes}:{seconds < 10 ? `0${seconds}` : seconds}
          </div>
        )}
      </div>
    </div>
  );
};

export default Timer;
