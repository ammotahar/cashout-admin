"use client";

import { useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import Modal from "@mui/material/Modal";
import Skeleton from "@mui/material/Skeleton";
import FormTitle from "./formTitle";
import Button from "./button";
import { getUserDetailsApi } from "@/services/users";
import { UserDetailsType } from "@/services/users/types";
import { rialSeparator } from "@/utils/rialSeperator";
import { jalaliConverter, miladiToJalali } from "@/utils/jalaliConverter";

const RowItem = ({
  title,
  value,
}: {
  title?: string;
  value?: string | React.ReactNode;
}) => {
  return (
    <>
      <div className="col-span-1 text-right">
        <span className="text-black text-sm">{title}</span>
      </div>

      <div className="col-span-1 text-right lg:text-left">
        <span className="text-gray text-sm">{value}</span>
      </div>
    </>
  );
};

export default function UserDetailsModal({
  open,
  onClose,
  userId,
}: {
  open: boolean;
  onClose?: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  userId: number;
}) {
  const [data, setData] = useState<UserDetailsType | undefined>();
  const [loading, setLoading] = useState(false);

  const router = useRouter();

  async function getUserDetails(id: number) {
    setLoading(true);

    const res = await getUserDetailsApi(id);

    if (res) {
      setData(res?.data);
    }

    setLoading(false);
  }

  useEffect(() => {
    if (open && userId) {
      getUserDetails(userId);
    }
  }, [userId, open]);

  return (
    <Modal
      open={open}
      onClose={onClose}
      className="!flex !items-center !justify-center"
    >
      {loading ? (
        <div className="bg-white flex flex-col items-center min-w-72 min-h-56 rounded-xl border-2 border-primary p-4 mx-4">
          <Skeleton variant="rounded" className="!w-full !h-96 !mt-4" />
        </div>
      ) : (
        <div className="bg-white flex flex-col items-center min-w-72 min-h-56 rounded-xl border-2 border-primary p-4 mx-4">
          <FormTitle
            title="مشاهده  جزئیات کاربر"
            className="w-full !text-center"
          />

          <div className="p-4 mt-4 bg-gray rounded-xl shadow w-full">
            <div className="grid grid-cols-1 lg:grid-cols-2 gap-4 max-h-96 overflow-auto p-4">
              <RowItem
                title="نام و نام خانوادگی:"
                value={`${data?.first_name} ${data?.last_name}`}
              />

              <RowItem title="کد ملی:" value={data?.national_code} />

              <RowItem title="شماره موبایل:" value={data?.mobile} />

              <RowItem title="نام کاربری:" value={data?.user_name} />

              <RowItem title="تلفن:" value={data?.phone} />

              <RowItem
                title="تاریخ تولد:"
                value={data?.birthdate && miladiToJalali(data?.birthdate)}
              />

              <RowItem
                title="تاریخ ساخت:"
                value={data?.create_time && jalaliConverter(data.create_time)}
              />

              <RowItem title="سمت سازمانی:" value={data?.job_position} />

              <RowItem
                title="سقف انتقال وجه داخلی:"
                value={data?.max_amount && rialSeparator(data?.max_amount)}
              />

              <RowItem
                title="سقف انتقال وجه ساتنا:"
                value={
                  data?.max_amount_satna && rialSeparator(data.max_amount_satna)
                }
              />

              <RowItem
                title="سقف انتقال وجه پایا:"
                value={
                  data?.max_amount_paya && rialSeparator(data.max_amount_paya)
                }
              />

              <RowItem
                title="وضعیت:"
                value={data?.status === true ? "فعال" : "غیر فعال"}
              />

              <RowItem title="نقش:" value={data?.role.title} />

              <div>
                <div className="col-span-1 text-right">
                  <span className="text-black text-sm">سطوح دسترسی:</span>
                </div>

                {data?.permission_code_list.map((item) => (
                  <div key={item.code} className="col-span-1 text-right">
                    <span className="text-gray text-sm">
                      {item.persian_title}
                    </span>
                  </div>
                ))}
              </div>
            </div>
          </div>

          <div className="grid grid-cols-2 lg:grid-cols-4 gap-4 mt-4 w-full">
            <Button
              fullRounded={false}
              className="!col-span-1 lg:!col-start-3"
              onClick={() => router.push(`/panel/users/create?id=${userId}`)}
            >
              ویرایش اطلاعات
            </Button>
            <Button
              variant="cancel"
              className="!col-span-1 lg:!col-start-4"
              onClick={(event) => onClose && onClose(event, "backdropClick")}
            >
              بستن
            </Button>
          </div>
        </div>
      )}
    </Modal>
  );
}
