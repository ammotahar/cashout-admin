"use client";

import { useEffect, useState, useCallback } from "react";
import Modal from "@mui/material/Modal";
import Skeleton from "@mui/material/Skeleton";
import FormTitle from "./formTitle";
import Button from "./button";
import {
  getCompanyWageDetailsApi,
  getBankWageDetailsApi,
} from "@/services/wage";
import {
  CompanyWageDetailsType,
  BankWageDetailsType,
  isCompanyWageDetailsType,
} from "@/services/wage/types";
import { rialSeparator } from "@/utils/rialSeperator";
import { jalaliConverter } from "@/utils/jalaliConverter";
import { getWageTypeTitle } from "@/utils/getWageTypeTitle";
import {
  CONSTANT,
  CONSTANT_TITLE,
  PERCENT,
  PERCENT_TITLE,
  LEVEL,
  LEVEL_TITLE,
} from "@/enums/wageTypeEnums";

const RowItem = ({
  title,
  value,
}: {
  title?: string;
  value?: string | React.ReactNode;
}) => {
  return (
    <>
      <div className="col-span-1 text-right">
        <span className="text-black text-sm">{title}</span>
      </div>

      <div className="col-span-1 text-right lg:text-left">
        <span className="text-gray text-sm">{value}</span>
      </div>
    </>
  );
};

export default function WageDetailsModal({
  open,
  onClose,
  wageId,
  type,
}: {
  open: boolean;
  onClose?: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  wageId: number;
  type: "company" | "bank";
}) {
  const [data, setData] = useState<
    CompanyWageDetailsType | BankWageDetailsType | undefined
  >();
  const [loading, setLoading] = useState(false);

  const getWageDetails = useCallback(
    async (id: number) => {
      setLoading(true);

      const res =
        type === "company"
          ? await getCompanyWageDetailsApi(id)
          : await getBankWageDetailsApi(id);

      if (res) {
        setData(res?.data);
      }

      setLoading(false);
    },
    [type]
  );

  useEffect(() => {
    if (open && wageId) {
      getWageDetails(wageId);
    }
  }, [wageId, open, getWageDetails]);

  return (
    <Modal
      open={open}
      onClose={onClose}
      className="!flex !items-center !justify-center"
    >
      {loading || data === undefined ? (
        <div className="bg-white flex flex-col items-center min-w-72 w-1/3 min-h-56 rounded-xl border-2 border-primary p-4 mx-4">
          <Skeleton variant="rounded" className="!w-full !h-96 !mt-4" />
        </div>
      ) : (
        <div className="bg-white flex flex-col items-center min-w-72 w-1/3 min-h-56 rounded-xl border-2 border-primary p-4 mx-4">
          <FormTitle
            title="مشاهده جزئیات کارمزد"
            className="w-full !text-center"
          />

          <div className="p-4 mt-4 bg-gray rounded-xl shadow w-full">
            <div className="grid grid-cols-1 lg:grid-cols-2 gap-4 max-h-96 overflow-auto p-4">
              {type === "company" && (
                <RowItem
                  title="نام شرکت:"
                  value={isCompanyWageDetailsType(data) && data?.company_name}
                />
              )}

              <RowItem
                title="مبلغ:"
                value={
                  data?.wage_type === CONSTANT
                    ? isCompanyWageDetailsType(data)
                      ? rialSeparator(data?.amount.toString())
                      : rialSeparator(data?.wage_amount.toString())
                    : isCompanyWageDetailsType(data)
                    ? data?.amount
                    : data?.wage_amount
                }
              />

              <RowItem
                title="تاریخ ثبت:"
                value={data?.create_time && jalaliConverter(data?.create_time)}
              />

              <RowItem title="نوع کارمزد:" value={data?.wage_type_title} />

              <RowItem
                title="نوع تراکنش:"
                value={data?.transaction_type_title}
              />

              <RowItem
                title="وضعیت:"
                value={data?.is_active === true ? "فعال" : "غیر فعال"}
              />

              {isCompanyWageDetailsType(data) && data?.wage_level_type && (
                <RowItem
                  title="نوع کارمزد پلکانی:"
                  value={getWageTypeTitle(data?.wage_level_type)}
                />
              )}

              {isCompanyWageDetailsType(data) && data?.wage_level !== null && (
                <div className="col-span-1 lg:col-span-2 w-full">
                  <FormTitle
                    title="سطوح کارمزد :"
                    className="w-full !text-center"
                  />

                  {data?.wage_level.map((item) => (
                    <div
                      key={item.id}
                      className="grid grid-cols-1 lg:grid-cols-2 gap-4 p-4 w-full"
                    >
                      <RowItem
                        title="مبلغ:"
                        value={rialSeparator(item?.amount.toString())}
                      />

                      <RowItem
                        title="از مقدار:"
                        value={rialSeparator(item?.from_value.toString())}
                      />

                      <RowItem
                        title="تا مقدار:"
                        value={rialSeparator(item?.to_value.toString())}
                      />

                      <hr className="col-span-1 lg:col-span-2 border-table" />
                    </div>
                  ))}
                </div>
              )}
            </div>
          </div>

          <div className="grid grid-cols-2 lg:grid-cols-4 gap-4 mt-4 w-full">
            <Button
              variant="cancel"
              className="!col-span-1 lg:!col-start-4"
              onClick={(event) => onClose && onClose(event, "backdropClick")}
            >
              بستن
            </Button>
          </div>
        </div>
      )}
    </Modal>
  );
}
