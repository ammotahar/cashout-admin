"use client";

import { useState } from "react";
import { ContextStore } from "./store";

export default function ContextProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  const [store, setStore] = useState();

  return (
    <ContextStore.Provider value={{ store, setStore }}>
      {children}
    </ContextStore.Provider>
  );
}
