export const NOT_CONFIRMED = 0;
export const CONFIRMED = 1;
export const AWAITING_CONFIRMATION = 2;
export const SENDING_TO_IPG = 3;
export const TRANSFER_TO_IPG_FAILED = 4;

export const NOT_CONFIRMED_TITLE = "تایید نشده";
export const CONFIRMED_TITLE = "تایید شده";
export const AWAITING_CONFIRMATION_TITLE = "در انتظار تایید";
export const SENDING_TO_IPG_TITLE = "ارسال شده به درگاه";
export const TRANSFER_TO_IPG_FAILED_TITLE = "خطا در ارسال به درگاه";
