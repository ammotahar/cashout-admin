export const PER_AMOUNT = 0;
export const PER_TRANSACTION_COUNT = 1;

export const PER_AMOUNT_TITLE = "به ازای هر مبلغ";
export const PER_TRANSACTION_COUNT_TITLE = "به ازای هر تعداد تراکنش";
