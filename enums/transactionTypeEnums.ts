export const SATNA = 0;
export const PAYA = 1;
export const INTERNAL = 2;
export const INQUIRY_IBAN = 3;

export const SATNA_TITLE = "ساتنا";
export const PAYA_TITLE = "پایا";
export const INTERNAL_TITLE = "انتقال داخلی";
export const INQUIRY_IBAN_TITLE = "استعلام شماره شبا";
