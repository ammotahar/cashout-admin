/** @type {import('next').NextConfig} */
const nextConfig = {
  env: {
    REACT_APP_BASE_URL: process.env.REACT_APP_BASE_URL,
    SERVER_BASE_URL: process.env.SERVER_BASE_URL,
  },
  images: {
    remotePatterns: [
      {
        protocol: "http",
        hostname: "37.152.177.31",
        port: "4004",
        pathname: "/**",
      },
    ],
  },
};

module.exports = nextConfig;
