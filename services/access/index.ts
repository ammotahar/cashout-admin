import Api from "services";

export const getRolesListApi = () => Api.GET({ url: "roles" });

export const getPermissionsListApi = () => Api.GET({ url: "permission" });
