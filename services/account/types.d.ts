export interface IbanInfoType {
  account_number: string;
  first_name: string;
  last_name: string;
  account_status: string;
  account_comment: string;
  bank: string;
}

export interface AccountListType {
  id: number;
  full_name: string;
  account_name: string;
  account_number: string;
  iban: string;
  bank_name: string;
}

export interface AccountDetailsType {
  id: number;
  bank_id: number;
  company_id: number;
  description: string;
  user_name: string;
  account_name: string;
  full_name: string;
  is_active: boolean;
  is_deleted: boolean;
  create_time: string;
  account_number: string;
  iban: string;
  customer_code: string;
  view_dashboard: boolean;
  account_number_inq: string;
  first_name_inq: string;
  account_status: string;
  user_id: number;
  bank_inq: string;
  channel_name: string;
  bank_name: string;
  password: string;
  secret_key: string;
}