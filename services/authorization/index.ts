import Api from "services";

export const sendOtpLoginApi = (data: {
  username: string;
  password: string;
  is_admin: boolean;
  service_type?: number;
}) => Api.POST({ url: "send_otp_login", data });

export const verifyOtpApi = (data: {
  username?: string | null;
  mobile?: string | null;
  is_admin: boolean;
  otp: string;
  service_type?: number;
}) => Api.POST({ url: "verify_otp", data });
