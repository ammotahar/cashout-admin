import Api from "services";

export const getChargeBalanceListApi = (query?: {
  amount?: number;
  bank_name?: string;
  from_date_insert_date?: string;
  to_date_insert_date?: string;
  from_iban?: string;
  to_iban?: string;
  ordering?: string;
  page_number?: number;
  per_page?: number;
  search?: string;
  status?: number;
  track_number?: number;
  type?: number;
}) => Api.GET({ url: "charge_balance", query });

export const getChargeBalanceDetailsApi = (id: number) =>
  Api.GET({ url: `charge_balance/${id}` });

export const confirmChargeBalanceApi = (data: {
  status: number;
  reject_result?: string;
  charge_balance_id: number;
}) => Api.POST({ url: "charge_balance/confirm_charge_balance", data });

export const getChargeBalanceListXlsxApi = (query?: {
  amount?: number;
  bank_name?: string;
  from_date_insert_date?: string;
  to_date_insert_date?: string;
  from_iban?: string;
  to_iban?: string;
  ordering?: string;
  search?: string;
  status?: number;
  track_number?: number;
  type?: number;
}) =>
  Api.GET({
    url: "charge_balance/get_charge_balance_list_xlsx",
    query,
    isBinary: true,
  });
