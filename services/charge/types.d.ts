export interface ChargeBalanceListType {
  id: number;
  amount: number;
  insert_date: string;
  bank_title: null | string;
  status: number;
  company_name: string;
  track_number: number;
  confirm_date: string | null;
  to_iban: string;
  from_iban: string;
  type: number;
}

export interface ChargeBalanceDetailsType {
  id: number;
  bank_title: null | string;
  confirmer_name: null | string;
  user_name: string;
  type_title: string;
  status_title: string;
  file_path: string | null;
  type: number;
  amount: number;
  confirmer_user_id: null | number;
  description: string;
  from_iban: string;
  to_iban: string;
  reject_result: string;
  insert_date: string;
  confirm_date: null | string;
  track_number: number;
  status: number;
  user: number;
  bank: null | number;
}
