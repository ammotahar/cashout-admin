import Api from "services";

export const getCompanyListApi = (query?: {
  code?: number;
  name?: string;
  page_number?: number;
  per_page?: number;
  search?: string;
  status?: boolean;
}) => Api.GET({ url: "company", query });

export const getCompanyListExcelApi = (query?: {
  code?: number;
  name?: string;
  search?: string;
  status?: boolean;
}) => Api.GET({ url: "company/get_company_list_xlsx", query, isBinary: true });

export const createCompanyApi = (data: {
  name: string;
  code: number;
  phone?: string;
  min_deposit: number;
  number_create_user?: number;
  status?: boolean;
  description?: string;
  wage_account_company?: string;
}) => Api.POST({ url: "company", data });

export const editCompanyApi = (
  data: {
    name?: string;
    code?: number;
    phone?: string;
    min_deposit?: number;
    number_create_user?: number;
    status?: boolean;
    description?: string;
    wage_account_company?: string;
  },
  id: number
) => Api.PATCH({ url: `company/${id}`, data });

export const getCompanyDetailsApi = (id: number) =>
  Api.GET({ url: `company/${id}` });

export const getBalanceAndMinDepositApi = () =>
  Api.GET({ url: "company/get_balance_and_min_deposit" });
