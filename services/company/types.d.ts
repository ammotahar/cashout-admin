export interface CompanyListType {
  id: number;
  name: string;
  code: number;
  phone: string;
  balance: number;
  update_date: string;
  min_deposit: number;
  number_create_user: number;
  status: boolean;
  description: string;
  wage_account_company: string;
}

export interface CompanyDetailsType {
  id: number;
  name: string;
  phone: string;
  balance: number;
  update_date: string;
  min_deposit: number;
  number_create_user: number;
  status: boolean;
  description: string;
  wage_account_company: string;
  company_code: string;
}
