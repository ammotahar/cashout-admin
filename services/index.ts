import { ServerErrorHandling } from "./errorHandling";
import queryBuilder from "utils/queryBuilder";

const ApiCreator = async ({
  data,
  headers,
  method,
  query = "",
  token,
  url,
  responseType,
  ott,
  hasErr = true,
  isRefresh,
  isFormData = false,
  isBinary = false,
  ott_token = undefined,
}: any) => {
  const Authorization_ =
    ott_token || localStorage.getItem("access_token") || token;
  // const language = store.getState().config.language;
  const customConfig = {
    headers: {
      "Accept-Language": "fa",
      ...(!isFormData && { "Content-Type": "application/json" }),
      ...(Authorization_ && { Authorization: `Bearer ${Authorization_}` }),
      ...(ott && { token: `${ott}` }),
      ...(headers && { ...headers }),
    },
    method: method,
    body: isFormData ? data : JSON.stringify(data),
    ...(responseType && { responseType: responseType }),
  };
  return await fetch(
    `${process.env.REACT_APP_BASE_URL}${url}${query && queryBuilder(query)}`,
    customConfig
  )
    .then(async (res) => {
      if (isBinary) {
        return res.blob();
      }
      if (res?.status >= 200 && res?.status <= 299) {
        if (res?.status === 204) {
          return;
        }
        return res.json();
      } else {
        return ServerErrorHandling(
          await res.json(),
          isRefresh,
          res?.status,
          "withServerData"
        );
      }
    })
    .catch(hasErr ? (err) => ServerErrorHandling(err, isRefresh) : () => {});
};

class Api {
  static GET = (props: any) => ApiCreator({ ...props, method: "GET" });

  static POST = (props: any) => ApiCreator({ ...props, method: "POST" });

  static PUT = (props: any) => ApiCreator({ ...props, method: "PUT" });

  static PATCH = (props: any) => ApiCreator({ ...props, method: "PATCH" });

  static DELETE = (props: any) => ApiCreator({ ...props, method: "DELETE" });
}

export default Api;
