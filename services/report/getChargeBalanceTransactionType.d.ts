export interface getChargeBalanceTransactionType {
  confirm_date: string;
  amount: number;
}
