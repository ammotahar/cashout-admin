import Api from "services";

export const getSettlementTransactionApi = (query?: {
  per_page?: number;
  page_number?: number;
}) => Api.GET({ url: "reports/get_settlement_transaction", query });

export const getChargeBalanceTransactionApi = () =>
  Api.GET({ url: "reports/get_charge_balance_transaction" });
