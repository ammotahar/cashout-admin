export interface ProfileType {
  first_name: string;
  last_name: string;
  mobile: string;
  phone: string;
  birthdate: string;
  national_code: string;
  user_name: string;
  job_position: string;
  create_time: string;
}

export interface UsersListType {
  id: number;
  first_name: string;
  last_name: string;
  national_code: string;
  mobile: string;
  user_name: string;
  status: boolean;
}

export interface UserDetailsType {
  id: number;
  company_id: number;
  status: boolean;
  first_name: string;
  last_name: string;
  mobile: string;
  phone: string;
  birthdate: string;
  national_code: string;
  create_time: string;
  user_name: string;
  bank_id: null | number;
  job_position: string;
  max_amount: null | string;
  max_amount_satna: null | string;
  max_amount_paya: null | string;
  role: {
    id: number;
    title: string;
    is_active: boolean;
    role_code: string;
  };
  permission_code_list: {
    code: string;
    english_title: string;
    persian_title: string;
  }[];
}
