import Api from "services";

export const getCompanyWageListApi = (query?: {
  page_number?: number;
  per_page?: number;
  amount?: number;
  company?: string;
  company_name?: string;
  create_time?: string;
  is_active?: boolean;
  search?: string;
  transaction_type?: string;
  wage_type?: string;
}) => Api.GET({ url: "company_wage", query });

export const getCompanyWageListXlsxApi = (query?: {
  amount?: number;
  company?: string;
  company_name?: string;
  create_time?: string;
  is_active?: boolean;
  search?: string;
  transaction_type?: string;
  wage_type?: string;
}) =>
  Api.GET({
    url: "company_wage/get_company_wage_list_xlsx",
    query,
    isBinary: true,
  });

export const getCompanyWageDetailsApi = (id: number) =>
  Api.GET({ url: `company_wage/${id}` });

export const deactiveCompanyWageApi = (
  data: {
    is_active: boolean;
  },
  id: number
) => Api.PATCH({ url: `company_wage/${id}`, data });

export const createCompanyWageApi = (data: {
  wage_type: 0 | 1 | 2;
  amount?: number;
  transaction_type: 0 | 1 | 2 | 3;
  is_active?: boolean;
  company: number;
  wage_level_type?: 0 | 1;
  wage_level?: {
    amount: number;
    from_value: number;
    to_value: number;
  }[];
}) => Api.POST({ url: "company_wage", data });

export const getBankWageListApi = (query?: {
  page_number?: number;
  per_page?: number;
  wage_amount?: number;
  create_time?: string;
  is_active?: boolean;
  transaction_type?: string;
  wage_type?: string;
}) => Api.GET({ url: "bank_wage", query });

export const getBankWageListXlsxApi = (query?: {
  wage_amount?: number;
  create_time?: string;
  is_active?: boolean;
  transaction_type?: string;
  wage_type?: string;
}) =>
  Api.GET({
    url: "bank_wage/get_bank_wage_list_xlsx",
    query,
    isBinary: true,
  });

export const getBankWageDetailsApi = (id: number) =>
  Api.GET({ url: `bank_wage/${id}` });

export const deactiveBankWageApi = (
  data: {
    is_active: boolean;
  },
  id: number
) => Api.PATCH({ url: `bank_wage/${id}`, data });

export const createBankWageApi = (data: {
  wage_type: 0 | 1;
  wage_amount: string;
  transaction_type: 0 | 1 | 2;
  is_active?: boolean;
  max_wage?: number;
  min_wage?: number;
  from_amount?: number;
  to_amount?: number;
  description?: string;
  restriction_type?: 0 | 1;
  restriction_value?: number;
  extra_wage_amount?: number;
}) => Api.POST({ url: "bank_wage", data });
