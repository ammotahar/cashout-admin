export interface CompanyWageListType {
  id: number;
  company_id: number;
  wage_type: number;
  amount: number;
  transaction_type: number;
  is_active: boolean;
  create_time: string;
  company_name: string;
  transaction_type_title: string;
  wage_type_title: string;
  wage_level_type: number;
  wage_level:
    | {
        id: number;
        company_wage_id: number;
        amount: number;
        from_value: number;
        to_value: number;
      }[]
    | null;
}

export interface CompanyWageDetailsType {
  id: number;
  company_id: number;
  wage_type: number;
  amount: number;
  transaction_type: number;
  is_active: boolean;
  create_time: string;
  company_name: string;
  transaction_type_title: string;
  wage_type_title: string;
  wage_level_type: number;
  wage_level:
    | {
        id: number;
        company_wage_id: number;
        amount: number;
        from_value: number;
        to_value: number;
      }[]
    | null;
}

export interface BankWageListType {
  id: number;
  user_id: number;
  wage_type: number;
  wage_amount: number;
  transaction_type: number;
  is_active: boolean;
  create_time: string;
  max_wage: null | number;
  min_wage: null | number;
  from_amount: null | number;
  to_amount: null | number;
  description: string;
  restriction_type: null | number;
  restriction_value: null | number;
  extra_wage_amount: null | number;
  transaction_type_title: string;
  wage_type_title: string;
  restriction_type_title: null | string;
}

export interface BankWageDetailsType {
  id: number;
  user_id: number;
  wage_type: number;
  wage_amount: number;
  transaction_type: number;
  is_active: boolean;
  create_time: string;
  max_wage: number;
  min_wage: null | number;
  from_amount: null | number;
  to_amount: null | number;
  description: string;
  restriction_type: null | number;
  restriction_value: null | number;
  extra_wage_amount: null | number;
  transaction_type_title: string;
  wage_type_title: string;
  restriction_type_title: null | string;
}

export function isCompanyWageDetailsType(
  object: any
): object is CompanyWageDetailsType {
  return "company_id" in object;
}
