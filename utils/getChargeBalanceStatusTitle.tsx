import {
  NOT_CONFIRMED,
  NOT_CONFIRMED_TITLE,
  CONFIRMED,
  CONFIRMED_TITLE,
  AWAITING_CONFIRMATION,
  AWAITING_CONFIRMATION_TITLE,
  SENDING_TO_IPG,
  SENDING_TO_IPG_TITLE,
  TRANSFER_TO_IPG_FAILED,
  TRANSFER_TO_IPG_FAILED_TITLE,
} from "@/enums/chargeBalanceStatusEnums";

export function getChargeBalanceStatusTitle(status: number) {
  switch (status) {
    case NOT_CONFIRMED:
      return <span className="text-error">{NOT_CONFIRMED_TITLE}</span>;

    case CONFIRMED:
      return <span className="text-success">{CONFIRMED_TITLE}</span>;

    case AWAITING_CONFIRMATION:
      return <span className="text-yellow">{AWAITING_CONFIRMATION_TITLE}</span>;

    case SENDING_TO_IPG:
      return <span className="text-yellow">{SENDING_TO_IPG_TITLE}</span>;

    case TRANSFER_TO_IPG_FAILED:
      return <span className="text-error">{TRANSFER_TO_IPG_FAILED_TITLE}</span>;

    default:
      return "نا مشخص";
  }
}
