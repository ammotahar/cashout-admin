import {
  MANUAL,
  MANUAL_TITLE,
  IPG,
  IPG_TITLE,
} from "@/enums/chargeBalanceTypeEnums";

export function getChargeBalanceTypeTitle(status: number) {
  switch (status) {
    case MANUAL:
      return MANUAL_TITLE;

    case IPG:
      return IPG_TITLE;

    default:
      return "نا مشخص";
  }
}
