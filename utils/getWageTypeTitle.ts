import {
  CONSTANT,
  CONSTANT_TITLE,
  PERCENT,
  PERCENT_TITLE,
  LEVEL,
  LEVEL_TITLE,
} from "@/enums/wageTypeEnums";

export function getWageTypeTitle(wageType: number) {
  switch (wageType) {
    case CONSTANT:
      return CONSTANT_TITLE;

    case PERCENT:
      return PERCENT_TITLE;

    case LEVEL:
      return LEVEL_TITLE;

    default:
      return "نا مشخص";
  }
}
