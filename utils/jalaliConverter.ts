import moment from "jalali-moment";

export function jalaliConverter(date: string) {
  return moment(date, "YYYY-M-DTHH:mm").format("HH:mm jYYYY/jM/jD");
}

export function miladiToJalali(date: string) {
  if (date.length > 0) {
    return moment(date, "YYYY-M-DTHH:mm").format("jYYYY/jM/jD");
  }
  return "";
}

export function getJalaliDay(date: string) {
  const day = moment(date, "YYYY-M-DTHH:mm").format("dddd");
  switch (day) {
    case "Sunday":
      return "یکشنبه";

    case "Monday":
      return "دوشنبه";

    case "Tuesday":
      return "سه شنبه";

    case "Wednesday":
      return "چهارشنبه";

    case "Thursday":
      return "پنج شنبه";

    case "Friday":
      return "جمعه";

    case "Saturday":
      return "شنبه";

    default:
      break;
  }
}
